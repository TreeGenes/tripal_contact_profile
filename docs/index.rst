.. Tripal Contact Profile documentation master file, created by
   sphinx-quickstart on Wed Aug 22 21:15:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tripal Contact Profile's documentation!
==================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents

   introduction
   installation
   migration
   administration
   user
   developer   
   orcid
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
