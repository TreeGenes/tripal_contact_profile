Administration
==================================================
Tripal Contact Profile module contains a built-in user interface within the Drupal administration pages that
a site owner / administrator can use to configure various features. To access the administration pages, you must be an
administrator and logged in. You can then visit /admin/tripal/tripal_contact_profile/install to access the first TCP 
related admin page. Continue reading this section for more details.

==================================================
1.1 Install / Uninstall Options
==================================================
To gain access to the TCP Configuration Section, go to the website's admin section and select 'Tripal' from the top dashboard.

.. image:: images/admin_tripal_section.png

From there, click on 'Tripal Contact Profile Configuration'

.. image:: images/admin_tripal_section_tcp_configuration.png

Alternatively, you can go directly to the following url to get directly to the Tripal Contact Profile Configuration section.
Location: /admin/tripal/tripal_contact_profile/install

There are 4 vertical tabs to simplify the Install / Uninstall Options page

* Installation
* Migration
* Maintenance
* Uninstall Options

.. image:: images/admin_tripal_section_config_tabs.png

==================================================
1.1.1 Installation
==================================================

This section does some basic actions to get TCP working on a website. The following labelled buttons give you some flexibility
regarding the fields that will be created for your users to create their profiles.

* Install
You must begin here. This will install most basic fields such as first name, last name, location related fields, 
position fields etc. This will create the Tripal Content Type and its corresponding basic fields. 
The other options below will not work properly without running this install procedure.

* Create Photo Field
This will create an additional field that will allow users to also upload a photo in their profile.


* Remove Photo Field
This will remove the photo field which thereby disables users from upload photos to their profile. 

* Create Basic User Registration Fields
This creates some basic fields in the actual user profile and therefore sounds confusing. This option is different
because it does not create fields within the TCP profile but rather the built-in user profile shipped with Drupal.
The built-in user profile is managed via /admin/config/people/accounts/fields while TCP profile is managed by clicking
on Tripal Contact Profile via /admin/structure/bio_data . The reason for these basic user registration fields was 
originally meant to be used to easily determine if a user was a spam account but it is also used now to keep more 
information in the built-in profile primarily to be able to recognize the user by First name and Last name as well 
as organization.

==================================================
1.1.2 Migration
==================================================

* Migrate Old Contacts to Tripal Contact Profile
This option does a one-time migration from the old Tripal V2 'Contact' to the new Tripal Contact Profile that
we have developed. It does so by converting the entities that were created into the new Tripal Contact Profile 
entity / bundle type. If you have a very large database filled with contacts, you may have to increase your PHP 
execution time for this to work successfully. We used a time of 10 minutes and then switched it back to 30 seconds 
once completed.

* Migrate all genus values that are integers into species text values
This option will probably not be important to you. This was designed particularly for TreeGenes since we had old data 
such as genus values that were really ids. This feature would convert the ids into actual species text values within the 
Tripal Contact Profile.


==================================================
1.1.3 Maintenance
==================================================

* Check for new fields
This option can be used from time to time. It tells the Tripal Contact Profile to look for new fields that might have been 
created. New fields can be created by adding data to the chado.contactprops table manually once proper type_ids exist. This 
function would make that new field be available in the Tripal Contact Profile manage fields section.

* Clear Caches
This just a convenient way to clear Drupal cache.

==================================================
1.1.4 Uninstall Options
==================================================

* Uninstall (only dummy record)
A dummy record is usually created when you first click the install button above to create basic fields. This is how 
Tripal is able to detect the fields under the Manage Fields section of the Tripal Contact Profile. If you don't have any Tripal 
Contact Profiles already created by users - this will essentially remove the detectable fields within the manage fields. This is 
mostly used by us for testing /demo purposes.

* Uninstall (all records)
Self explanatory. Be super careful. This will remove all data within the chado.contact table!

==================================================
1.2 Field Options
==================================================

The field options tab can be found to the top of the Tripal Contact Profile Configuration page.

.. image:: images/admin_tripal_section_top_tabs.png

Most options found in this section will add features to a field or allow you to control the type of field.

.. image:: images/admin_tripal_section_field_options_vertical.png

==================================================
1.2.1 ORCID Integration
==================================================

* Enable ORCID auto-populate button under field
Selecting this option will cause a 'Populate' button right under the ORCID field TCP form. This populate button 
will attempt to pull data from orcid.org and automatically enter the data into the corresponding fields.

==================================================
1.2.2 Contact Profile Form Options
==================================================

* Perform basic validation of defaults fields (email, websites) in Tripal Contact Profile
Enabling this option will perform basic validaiton to make sure these fields adhere to proper formats. It is 
basically used to make sure data entered by the user is useful and error free to a higher degree.

* Remove descriptions from the submit/edit form
This will remove unnecessary descriptions from fields to make the TCP form a little more clean and presentable.

==================================================
1.2.3 Contact Profile Field Options
==================================================

* Autodetect email by using Drupal user email
This is a useful function to reduce the user's effort of having to retype their email into the form. It does so 
by pulling their email address from their user account which should already have an email set.

* Hide description field
This is a chado related field that is not necessary. This removes the field since it isn't used to store any useful 
information.

* Switch position text field into select field
Selecting this option will convert the position text field into a select list with built-in options already added.

* Switch organization text field into autocomplete field
Selecting this option will convert the organization text field into an autocomplete field. Autocomplete field 
looks up organizations from all other users or you can type in your new organization right in the autocomplete 
field.

* Switch country text field into select field
Selecting this option will convert the text field into a select list with a list of already prepolulated countries. 
This reduces the chance of a user making any data entry errors. Highly recommended.

* Switch state text field into select field
Selecting this option will convert the state field into a select list field. The state field is used for only 
United States or Canada. This option will populate the state select list with the states within the United States 
or Canada provinces depending on the country that was selected. This is also highly recommended.

* Convert URL field from a text field to a compound field (select list and text field)
This option will convert url text fields into a dual field which accepts url type and actual url location.

* Convert URL field from a single field to a multivalue field
This option will convert a single url field into a multi value field which allows multiple urls to be submitted.

* Convert Genus field from a single field to a multivalue field
This option will convert a genus field into a multi value field which allows multiple genus to be submitted.

* Convert Genus text field(s) into dual Genus and Species select fields
This option will convert a genus text field into a dual field that accepts both  Genus and Species options.

* Convert Research Interest field from a single field to a multivalue field
This option will convert the research interest single field into a multi value field which allows multiple values.

==================================================
1.2.4 Contact Profile Label Options
==================================================

* Set sub-labels for Research Interest to another label
You can change the label for Research Interest field(s) by entering alternate text

* Set sub-labels for Genus to another label
You can change the label for Genus field(s) by entering alternate text

* Set sub-labels for Url to another label
You can change the label for Url field(s) by entering alternate text

==================================================
1.2.5 User Registration Options
==================================================

* Redirect the Register tab in /user to point to the register multistep form
This option will redirect the user registration to the multi step registration version that includes both the 
user registration form and then the TCP form creation.

* Add Tripal Contact Profile submission form to User Registration form
This was the intial implementation of getting the TCP registration on the user registration form. This is no longer 
supported but left as an alternative to the above multi registration form. Do not use this option if the multi 
step registration form option above is already enabled.

* Enable automatic population of Full name, Organization and Country from user registration
This will pull user data already within the user account into the TCP corresponding fields to avoid 
having to re-enter the data twice. Very convenient.

* Perform basic validation on Email and Full name field on User Registration page
This option will do basic validation to make sure proper formatting of user data is checked. A warning will be 
displayed to the user if they do not enter valid data.

==================================================
1.3 Linker Options
==================================================

.. image:: images/admin_tripal_section_top_tabs.png

* Enable Edit Profile menu item in User Menu
Adds a special Edit Profile link in the User Menu. If clicked, this link will try to connect to the current 
logged in user's TCP profile.

* Enable Contact login linker notices for all /user/* pages
This adds notices to all pages in the /user part of the url to alert users to create or edit their TCP profiles.

* Enable Contact Profile tab with edit and view links
This creates a Contact Profile tab within the /user section of Drupal with links to create or edit the logged in
 user's TCP profile.

==================================================
1.4 Form field ordering
==================================================

TCP uses the Tripal and Drupal APIs as much as possible and thus the TCP fields that users will user to enter data 
can be ordered by going to Admin -> Structure -> Tripal Content Types -> Tripal Contact Profile and then going to 
to the Manage Fields tab.

Go to the top admin dashboard bar:

.. image:: images/admin_tripal_section.png

Select 'Tripal Content Types':

.. image:: images/admin_structure_section_tct.png

Choose manage fields under the Tripal Contact Profile type:

.. image:: images/admin_structure_section_tcp_type.png

Here you can select the drag icon to move fields up and down:

.. image:: images/admin_structure_section_tcp_managefields.png

