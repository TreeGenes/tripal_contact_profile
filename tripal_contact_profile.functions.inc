<?php
/**
 * @file
 * Functions integration for the Tripal Contact Profile module.
 */


/**
 * Helper function to get current user Tripal Contact Profile Entity number
 *
 * @return int 
 *  Returns entity_id as int
 *
 */
function tripal_contact_profile_get_contact_entity_id() {
			global $user;
			$user_email = @strtolower($user->mail);
	
			//dpm($user_email);
			//We need to query chado contacts to see if there's a record that contains this email, we then get the contact_id
			$args = array(
				':user_email' => $user_email,
			);
			$where_clause = "CONTACTPROP.contact_id = CONTACT.contact_id AND CVTERM.cvterm_id = CONTACTPROP.type_id AND  
			CVTERM.name ILIKE 'email' AND CVTERM.definition ILIKE 'Email address' AND CONTACTPROP.value ILIKE :user_email ";
			$sql = "SELECT CONTACT.contact_id AS contact_id_1, CONTACT.name as contact_name, CONTACTPROP.*, CVTERM.* 
				FROM chado.contact AS CONTACT, chado.contactprop AS CONTACTPROP, chado.cvterm AS CVTERM 
				WHERE $where_clause ORDER BY contact_id_1 DESC LIMIT 1;";
			//dpm($sql);
			
			//dpm($sql2);
			
			$result = chado_query($sql, $args);
			
			//dpm($result);
			//dpm($result->rowCount());
			
			/* Check whether there is a record found from the query that contains the email address */
			//dpm($result->rowCount());
			if($result->rowCount() == 1) {
				foreach($result as $r) {//for each is only to iterate, since there is only 1 record, this doesn't pose a problem
					//dpm($r);
					
					/* We now need to get the entity_id from the Drupal table chado_ tripal_contact_profile_getMachineName() */
					$args = array(
						':contact_id_1' => $r->contact_id_1,
					);
					$result_entity_id = chado_query("SELECT entity_id FROM chado_" . tripal_contact_profile_getMachineName() ." WHERE record_id=:contact_id_1 LIMIT 1;", $args);
					foreach($result_entity_id as $r_entity_id) {
						//dpm($r_entity_id);
						$entity_id = $r_entity_id->entity_id;
						return $entity_id;
					}
				}
				return -1;
			}
			return -1;
}

/**
 * Helper function to get Tripal Contact Profile Entity number from contact_id
 *
 * @param string $contact_id
 *   The record contact_id
 *
 * @return null
 *  Returns int $entity_id or -1 if not found
 *
 */
function tripal_contact_profile_get_contact_entity_id_from_contact_id($contact_id) {
	$args = array(
		':contact_id' => $contact_id,
	);
	$result_entity_id = chado_query("SELECT entity_id FROM chado_" . tripal_contact_profile_getMachineName() ." WHERE record_id=:contact_id LIMIT 1;", $args);
	foreach($result_entity_id as $r_entity_id) {
		//dpm($r_entity_id);
		$entity_id = $r_entity_id->entity_id;
		return $entity_id;
	}
	return -1;
}


/**
 * This helper function is a highly convenient function that tries to find the
 * actualy id_no of the bundle from it's human readable label
 *
 *
 * @return int
 *  Returns int with the profile_id
 *
 */
function tripal_contact_profile_id() {
	$list = tripal_get_content_types();
	foreach ($list as $item) {
		//dpm($item);
		//dpm($item->label);
		if($item->label == "Tripal Contact Profile") {
			//dpm($item);
			return $item->id;
		}
	}
}

/**
 * This helper function is a highly convenient function that tries to find the
 * actualy machine_name of the bundle from it's human readable label
 *
 *
 * @return string
 *  Returns machine name
 *
 */
function tripal_contact_profile_getMachineName() {
	$list = tripal_get_content_types();
	foreach ($list as $item) {
		//dpm($item);
		//dpm($item->label);
		if($item->label == "Tripal Contact Profile") {
			//dpm($item);
			return $item->name;
		}
	}
}

/**
 * This helper function is a highly convenient function that tries to find the
 * actualy machine_name ID number of the bundle from it's human readable label
 *
 *
 * @return string
 *  Returns string machinename_id
 *
 */
function tripal_contact_profile_getMachineName_id() {
	$list = tripal_get_content_types();
	foreach ($list as $item) {
		//dpm($item);
		//dpm($item->label);
		if($item->label == "Tripal Contact Profile") {
			$underscore_parts = explode("_", $item->name);
			return $underscore_parts[2];
		}
	}
}



/**
 * This is a helper function used to get the cvterm_id by using the cv_name and cvterm_name
 *
 * @param string $cv_name
 *   The cv_name
 * @param string $cvterm_name
 *   The cvterm_name 
 *
 * @return int
 *  Returns cvterm_id or null
 *
 */
function tripal_contact_profile_get_cvtermid($cv_name, $cvterm_name) {
	/* Find the DB name which is the in cv table */
	$args = array(
		':cv_name' => $cv_name,
	);
	$sql = "SELECT cv_id FROM chado.cv WHERE name ILIKE :cv_name LIMIT 1;";
	$results = chado_query($sql, $args);
	$row_count = $results->rowCount();
	//If cvname is found
	if($row_count > 0) {
		
		//Foreach is actually just to get the first record since we already limit the SQL statement to 1
		foreach($results as $row) {
			//dpm($row);
			$db_id = $row->cv_id;
		}
		
		//Now we need to search for the cvterm_name with cv_id = $db_id
		$args = array(
			':cvterm_name' => $cvterm_name,
			':db_id' => $db_id,
		);
		$sql = "SELECT * FROM chado.cvterm WHERE cv_id = :db_id AND name ILIKE :cvterm_name LIMIT 1";
		$results = chado_query($sql, $args);
		$row_count = $results->rowCount();
		
		//If cvterm_name is found
		if($row_count > 0) {
			foreach($results as $row) {
				return $row->cvterm_id;
			}
		}
		else {
			drupal_set_message("Fail: Could not find the cvterm: " . $cvterm . "even though the db: $db was found");
		}
		
	}
	else {
		drupal_set_message("Error: Could not find the cvterm: " . $cvterm . ", particularly the db: $db");
		return null;
	}
}



?>