User
=======
This user manual section focuses on user related visuals offered by the Tripal Contact Profile module. The screenshots may vary a little depending on whether 
new features have been added to the module but should generally be recognizable.

======================
1. Multi Registration
======================
The multi registration form once enabled allows anonymous users of the Tripal website to register. There are 2 main registration page steps:
* User account
* Details

We've included a status bar to the top of the multi step registration page to show you what page you are on and which pages you have successfully completed as seen in the image below.

.. image:: images/status_bar.png

=========================
1.1 Step 1 - User account
=========================
The 'User account' page is seen first and allows you to enter basic information about yourself. The data you enter in here will be saved under your 
Drupal user account - this means, it is part of the core of Drupal and is thus required to create your user login (username/password) for the website. This is standard for all Drupal/Tripal websites.
After you have completed entering the required information, you must click 'Continue to Step 2' to move on to the second page (Details).

.. image:: images/registration_page_example.png

====================
1.2 Step 2 - Details
====================

The 'Details' page, which can be seen after you click the 'Continue to Step 2' button, allows you to enter your Tripal Contact Profile details. 
This is really where your Tripal Contact Profile is created. Some fields on this page will be automatically populated based on what you entered 
in the 'User account' page previously seen to avoid you having to re-enter similar information. 

.. image:: images/registration_page2_example.png

* ORCID 
On this details page, if you already have an ORCID, you can speed up the process of entering your data by importing your orcid profile. 
To do this, enter your ORCID into the orcid field and click the 'Populate ORCID Information' button. This will import information such as your name, organization name, organization address 
and doi's of your published work into the relevant fields. 

.. image:: images/registration_page2_orcid_feature.png

When you've finished entered your information, you can submit all your details by clicking on the 'Complete Registration' button. You will then see a confirmation message signifying that you have successfully registered.

.. image:: images/registration_page2_completebutton.png

============================
2. Logging into your account
============================
Once you have successfully registered, you should now be able to log into the website. You can login to a Drupal/Tripal website by visiting the http://domain/user/login page
for the website using your new username and password. Remember to replace 'domain' with the website's real domain name. A login page will look similar to the following screenshot:

.. image:: images/login_page.png

=============================
3. Viewing your Profile (TCP)
=============================
To view your personal Tripal Contact Profile page, visit your Drupal/Tripal account details via the http://domain/user link. Remember to replace domain with the website's real domain. You should see 
a notice indicating that your contact details have been connected to your user account and particularly a 'View details' link. Click that link to view your profile page.

.. image:: images/view_edit_profile.png

Please note that depending on your theme, the above screenshot may look different but the content/text should not change.


=============================
4. Editing your Profile (TCP)
=============================
To edit your personal Tripal Contact Profile page, visit your Drupal/Tripal account details via the http://domain/user link. Remember to replace domain with the website's real domain. You should see 
a notice indicating that your contact details have been connected to your user account and particularly an 'Edit details' link. Click that link to view your profile page.

.. image:: images/view_edit_profile.png

ALSO, you can edit your profile using the convenient domain link http://domain/editprofile link. Remember to replace domain with the website's real domain name. This link is also available under the
User menu.

.. image:: images/usermenu_view_edit_profile.png

Please note that depending on your theme, the above screenshot may look different but the content/text should not change.


