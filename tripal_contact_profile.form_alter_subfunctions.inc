<?php

/**
 * Helper function to convert url text field into a compound field
 *
 * @param array $form
 *   The form array
 *
 * @param array $form_state
 *   The form state
 *
 * @return null
 *  Returns nothing
 *
 */
function tripal_contact_profile_url_convert_to_compoundfield(&$form, &$form_state) {
	if(variable_get('contact_url_convertmultivalue_setting') == 1) {
		//always +1 fields since Drupal/Chado adds an extra field to add new data
		$url_field_count = $form_state['field']['local__url']['und']['items_count'] + 1; 
	}
	else {
		//if it's a single value
		$url_field_count = 1;
	}
	
	//Go through each field
	$form['local__url']['und']['#title'] = variable_get('contact_url_label_setting', 'Url');
	for($i=0; $i < $url_field_count; $i++) {
		/*
		$form['local__url']['und'][$i]['label'] = array(
			'#type' => 'label',
			'#prefix' => '<label>' . variable_get('contact_url_label_setting', 'Url') . '</label>',
		);
		*/
		/*
		else {
			$form['local__url']['und'][$i]['label'] = array(
				'#type' => 'label',
				'#prefix' => '<label>' . $form['local__url']['und'][$i]['chado-contactprop__value']['#title'] . '</label>',
			);
		}
		*/
		
		$url_type_options = array(
			'ResearchGate' => 'ResearchGate', 
			'LinkedIn' => 'LinkedIn', 
			'LabSite' => 'LabSite', 
			'Other' => 'Other',
		);
		
		$form['local__url']['und'][$i]['url_type'] = array(
			'#type' => 'select',
			'#input' => true,
			'#empty_option' => '- ' . t('Website Type') . ' -',
			'#options' => $url_type_options,
			'#title' => t('Website Type'),
			'#title_display' => 'invisible',
			'#theme_wrappers' => array(),
		);
		
		$form['local__url']['und'][$i]['url'] = array(
			'#type' => 'textfield',
			'#input' => true,
			'#empty_option' => '',
			'#title' => t('URL'),
			'#title_display' => 'invisible',
			'#theme_wrappers' => array(),
		);
		
		//Convert the original text field into a hidden field, since the value of this field
		//is still what is being submitted
		$form['local__url']['und'][$i]['chado-contactprop__value']['#type'] = 'hidden';
		
		//dpm($form['local__url']['und'][$i]['value']['#value']);
		
		//We have to cater for editing if the field already has a value in the text field
		if(@$form['local__url']['und'][$i]['value']['#value'] != "") {
			$value_parts = explode(" ", $form['local__url']['und'][$i]['value']['#value']);
			$url_type = @$value_parts[0];
			$url = @$value_parts[1];
			$form['local__url']['und'][$i]['url_type']['#default_value'] = @$value_parts[0];
			$form['local__url']['und'][$i]['url']['#default_value'] = @$value_parts[1];
		}
	}
	
	//Disable AJAX for Add more button, it just messes up the form items
	//Unfortunately, I haven't found a way to hook into the AJAX function
	//Every time AJAX is executed, the fields get reset to normal text fields
	//and in this case, we don't want that to happen.
	unset($form['local__url']['und']['add_more']['#ajax']);
}

/*
This converts basic genus text field into a 'compound' field
which contains 2 select lists - genus and species
and convert the text field into a hidden field which we still
use to hold the value
*/
/**
 * Helper function to convert genus text field into select list
 *
 * @param array $form
 *   The form array
 *
 * @param array $form_state
 *   The form state
 *
 * @return null
 *  Returns nothing
 *
 */
function tripal_contact_profile_genus_convert_to_selectlists(&$form, &$form_state) {
	if(isset($form_state['field']['local__genus'])){
		if(variable_get('contact_genus_convertmultivalue_setting') == 1) {
			//always +1 fields since Drupal/Chado adds an extra field to add new data
			$genus_field_count = $form_state['field']['local__genus']['und']['items_count'] + 1; 
		}
		else {
			//if it's a single value
			$genus_field_count = 1;
			//dpm($form['local__genus']['und'][0]);
		}
		
		//Get Genus options in advance
		$args = array();
		$genus_value_results = chado_query('SELECT DISTINCT(genus) FROM chado.organism ORDER BY genus ASC;', $args);
		$genus_options = array();
		foreach($genus_value_results as $genus_row) {
			$genus_options[$genus_row->genus] = $genus_row->genus;
		}
		
		//dpm(array_keys($form_state));
		//dpm($form_state['input']);
		//Go through each field
		$form['local__genus']['und']['#title'] = variable_get('contact_genus_label_setting', 'Genus');
		for($i=0; $i < $genus_field_count; $i++) {
			//Label gets lost, recreate
			/*
			$form['local__genus']['und'][$i]['label'] = array(
				'#type' => 'label',
				'#prefix' => '<label>' . variable_get('contact_genus_label_setting', 'Genus') . '</label>',
			);
			*/
			
			
			$submit_value = @$form['local__genus']['und'][$i]['value']['#value'];
			if($submit_value == "") {
				$submit_value = @$form_state['values']['local__genus']['und'][$i]['chado-contactprop__value']['#default_value'];
			}
			
			//This happens when add another item is clicked from another multifield etc.
			if($submit_value == "") {
				$submit_value = trim(@$form_state['input']['local__genus']['und'][$i]['genus'] . " " . @$form_state['input']['local__genus']['und'][$i]['species']);
			}
			//dpm("FORM ALTER");
			//dpm("SUBMIT VALUE:");
			//dpm($submit_value);
			//dpm($form_state['values']['local__genus']['und'][$i]);
			//dpm($form['local__genus']['und'][$i]);
			$submit_value_parts = explode(' ', $submit_value);
			$genus_value = @$submit_value_parts[0];
			$species_value = @$submit_value_parts[1];
			
			
			
			//Genus select list
			$form['local__genus']['und'][$i]['genus'] = array(
				'#type' => 'select',
				'#input' => true,
				'#empty_option' => '- ' . t('Genus') . ' -',
				'#options' => $genus_options,
				'#title' => t('Genus'),
				'#title_display' => 'invisible',
				'#theme_wrappers' => array(),
				'#attributes' => array('onChange' => "loadSpeciesAJAX($i);")
			);
			
			
			$species_options = array();
			if($species_value != "" && $species_value != "Species") {
				//dpm($species_value);
				$species_options = array(
					'- ' . t('Species') . ' -' => '- ' . t('Species') . ' -',
					"$species_value" => "$species_value",
				);
			}
			elseif(@$form_state['values']['local__genus']['und'][$i]['species'] != ""){
				//this happens on add new item, which reloads the page
				$species_value = $form_state['values']['local__genus']['und'][$i]['species'];
				$species_options = array("$species_value" => "$species_value");
			}
			
			//Species select list
			$form['local__genus']['und'][$i]['species'] = array(
				'#type' => 'select',
				'#input' => true,
				'#empty_option' => '- ' . t('Species') . ' -',
				'#options' => $species_options,
				'#default_value' => $species_value,
				'#title' => t('Species'),
				'#title_display' => 'invisible',
				'#validated' => true,
				'#theme_wrappers' => array(),
			);
			

			
			//Convert the original text field into a hidden field, since the value of this field
			//is still what is being submitted
			$form['local__genus']['und'][$i]['chado-contactprop__value']['#type'] = 'hidden';
			
			//dpm($form['local__genus']['und'][$i]['value']['#value']);
			
			//We have to cater for editing if the field already has a value in the text field
			if(@$form['local__genus']['und'][$i]['value']['#value'] != "") {
				//Check to see if the value is an integer, then look up the organism (noticed this on TreeGenes)
				/*
				$value = intval(@$form['local__genus']['und'][$i]['value']['#value']);
				if($value != 0) {
					//it's an int alright!
					$organism_result = chado_query("SELECT * FROM chado.organism WHERE organism_id = $value LIMIT 1;", array());
					foreach($organism_result as $organism_row) {
						//this should only happen once since the about has a limit of 1
						dpm($organism_row);
						$form['local__genus']['und'][$i]['value']['#value'] = $organism_row->genus . " " . $organism_row->species;
					}
				}
				*/
				
				
				$value_parts = explode(" ", $form['local__genus']['und'][$i]['value']['#value']);
				$genus = $value_parts[0];
				$species = @$value_parts[1];
				$form['local__genus']['und'][$i]['genus']['#default_value'] = $value_parts[0];
				$form['local__genus']['und'][$i]['species']['#default_value'] = @$value_parts[1];
			}
			
			//dpm($form['local__genus']['und'][$i]);
		}
		
		//Disable AJAX for Add more button, it just messes up the form items
		//Unfortunately, I haven't found a way to hook into the AJAX function
		//Every time AJAX is executed, the fields get reset to normal text fields
		//and in this case, we don't want that to happen.
		unset($form['local__genus']['und']['add_more']['#ajax']);
	}
}

/*
Populate Name, Organization and Location from User Registration
*/
/**
 * Helper function to convert url text field into a compound field
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_populateFromUserRegistration($form) {
	global $user;
	$account = user_load($user->uid);
	//dpm($form['tcontact__organization']['und'][0]);
	//dpm($account);
	//check to see if this is an admin account, in which case, we don't want the email to override
	if(!in_array('administrator',$account->roles)) {
		//dpm($account->tcp_fullname['und'][0]['value']);
		if(@isset($account->tcp_fullname['und'][0]['value']) && @$account->tcp_fullname['und'][0]['value'] != "") {
			$form['schema__name']['und'][0]['value']['#default_value'] = $account->tcp_fullname['und'][0]['value'];
		}
		
		if(@isset($account->tcp_organization['und'][0]['value']) && @$account->tcp_organization['und'][0]['value'] != "") {
			//dpm($account->tcp_organization['und'][0]['value']);
			$form['tcontact__organization']['und'][0]['chado-contactprop__value']['#default_value'] = $account->tcp_organization['und'][0]['value'];
		}
		
		if(@isset($account->tcp_country['und'][0]['value']) && @$account->tcp_country['und'][0]['value'] != "") {
			$form['tcontact__country']['und'][0]['chado-contactprop__value']['#default_value'] = $account->tcp_country['und'][0]['value'];
		}
		
		//dpm($form['schema__name']);		
	}
	else {
		drupal_set_message("Admin detected | Editing mode enabled | Disabled auto-populate from user registration to avoid admin data overwritting this user's data", 'warning');
	}
	

	
	return $form;
}


/*
Hide the description field
*/
/**
 * Helper function to hide description field
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_description_hide($form) {
	
	$css = "
		<style>
		#edit-schema-description {
			visibility: hidden;
			height: 0px;
		}
		</style>
	";
	
	$form['schema__description']['#prefix'] = $css;
	
	return $form;
}

/*
Autopopulate ORCID field
*/
/**
 * Helper function to add function to autopopulate ORCID
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_orcid_autopopulate($form) {
	if(isset($form['local__orcid'])) {
			drupal_add_js('https://cdn.jsdelivr.net/npm/sweetalert', 'external');
			$form['local__orcid']['#prefix'] = "<style>
			#edit-local-orcid-und-0-chado-contactprop-value {
				display: inline-block;
			}
			
			/*
			#button_populate_orcid {
				margin-left: 5px;
				background-color: #e2b448;
				padding: 8px;
				color: #FFFFFF;
				border-radius: 5px;
			}
			*/
			</style>			
			<a name='orcid'></a>
			";
			$form['local__orcid']['#suffix'] = "<div id='button_populate_orcid_container'><a href='#' id='button_populate_orcid' onclick='load_ORCID();'>Populate ORCID information</a></div>";
			$js = "";
			$js .= "
				jQuery(document).ready(function () {
					//var container = jQuery('.form-item-field-genus-und-' + i + '-chado-contactprop--value');
					//container.html(str + container.html())
				});
				
				function load_ORCID() {
					orcid = jQuery('#edit-local-orcid-und-0-chado-contactprop-value').val();
					//window.alert('OK');		
					
					jQuery.getJSON('../../ajax/load/orcid/' +  orcid, function( data ) {
					  //window.alert(data);
						

					  var items = [];
					  items.push('- Select Species -');
					  var populated = false;
					  jQuery.each(data, function( key, val ) {
						console.log(key + ':' + val);  
						items.push(key);
						//window.alert(val);
						if(val != '' && val != ' ' && val != null) {
							//if the value is not empty, set new values into the element but cater for name since we don't necessarily want to replace name
							if(key == 'edit-schema-name-und-0-value') {
								if(jQuery('#edit-schema-name-und-0-value').val() == '' || jQuery('#edit-schema-name-und-0-value').val() == ' ' || jQuery('#edit-schema-name-und-0-value').val() == null) {
									jQuery('#' + key).val(val);
								}
							}
							else if(key == 'edit-local-firstname-und-0-chado-contactprop-value') {
								if(jQuery('#edit-local-firstname-und-0-chado-contactprop-value').val() == '' || jQuery('#edit-local-firstname-und-0-chado-contactprop-value').val() == ' ' || jQuery('#edit-local-firstname-und-0-chado-contactprop-value').val() == null) {
									jQuery('#' + key).val(val);
								}								
							}
							else if(key == 'edit-local-lastname-und-0-chado-contactprop-value') {
								if(jQuery('#edit-local-lastname-und-0-chado-contactprop-value').val() == '' || jQuery('#edit-local-lastname-und-0-chado-contactprop-value').val() == ' ' || jQuery('#edit-local-lastname-und-0-chado-contactprop-value').val() == null) {
									jQuery('#' + key).val(val);
								}
							}							
							else {
								jQuery('#' + key).val(val);
							}
							
						}
						if(val != null && val != '' && val != ', ' && val != ',' && val != ' ') {
							populated = true;
							//window.alert(val);
						}
					  });
					  if(populated) {
						  //window.alert('ORCID information has successfully been retrieved and added to this form');
						  swal('ORCID information received', 'Awesome! Valid ORCID information was retrieved and has now been added to the appropriate form fields!', 'success', {timer: 2500});
					  }
					  else {
						  //window.alert('ORCID information could not be retreived, please double check your ORCID number');
						  swal('ORCID could not retrieve data', 'Ouch! We could not retrieve data for this ORCID, please double check that you entered a valid ORCID or contact us, we\'ll try to help!', 'error');
					  }
					  
					});
				}
				

			
			";
			drupal_add_js($js, 'inline');
	}
	else {
		drupal_set_message('Could not find ORCID field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}
	return $form;
}

/*
Autodetect email field
*/
/**
 * Helper function to autodetect email
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_autodetectEmail($form) {
	if(isset($form['local__email'])) {
		global $user;

		//check to see if this is an admin account, in which case, we don't want the email to override
		if(!in_array('administrator',$user->roles)) {		
			//$form['local__email']['und'][0]['chado-contactprop__value']['value']['#value'] = $user->mail;
			$form['local__email']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
			$form['local__email']['und'][0]['chado-contactprop__value']['#default_value'] = @$user->mail;
			//$form['local__email']['und'][0]['chado-contactprop__value']['#attributes'] = array('readonly' => 'readonly');	
			//$form['local__email']['und'][0]['chado-contactprop__value']['#description'] .= " (AUTODETECTED)";
			//$form['local__email']['und'][0]['chado-contactprop__value']['#title'] .= " (AUTODETECTED)";
		}
		else {
			drupal_set_message("Admin detected | Editing mode enabled | Disabled autodetect email to avoid admin email overwritting user's email", 'warning');
		}
	}
	else {
		drupal_set_message('Could not find Email address field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}
	return $form;
}

/**
 * Helper function to position text field into select list
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_alterPositionFieldToSelectList($form) {
		
		$position_values = array(
			'' => '-Choose from below-',
			'Breeder' => 'Breeder',
			'Professor' => 'Professor', 
			'Researcher' => 'Researcher', 
			'Staff' => 'Staff',
			'Graduate Student' => 'Graduate Student', 
			'Undergraduate Student' => 'Undergraduate Student', 
			'Postdoctoral Scholar' => 'Postdoctoral Scholar',
		);
		
		//dpm($form['sep__position']['und'][0]);
		if(isset($form['sep__position'])) {
			$form['sep__position']['und'][0]['chado-contactprop__value']['#type'] = 'select';
			$form['sep__position']['und'][0]['chado-contactprop__value']['#options'] = $position_values;
			//check if the key is mixed up with the value. This happens for old fields in which the full
			//position is the value. 
			if(in_array($form['sep__position']['und'][0]['chado-contactprop__value']['#default_value'], $position_values)) {
				$key = array_search($form['sep__position']['und'][0]['chado-contactprop__value']['#default_value'], $position_values);
				$form['sep__position']['und'][0]['chado-contactprop__value']['#default_value'] = $key;
			}
		}
		else {
			drupal_set_message('Could not find Position field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
		}
		return $form;
}


/*
Add countries list to country field
*/
/**
 * Helper function to convert country text field into a select list
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_alterCountryFieldToSelectList($form) {
		global $countries;
		
		//dpm($form['tcontact__country']['und'][0]);
		if(isset($form['tcontact__country'])) {
			$form['tcontact__country']['und'][0]['chado-contactprop__value']['#type'] = 'select';
			$form['tcontact__country']['und'][0]['chado-contactprop__value']['#options'] = $countries;
			//check if the key is mixed up with the value. This happens for old fields in which the full
			//country name is the value. This module makes use of 2 or 3 alphabetical values instead (key for $countries)
			if(in_array($form['tcontact__country']['und'][0]['chado-contactprop__value']['#default_value'], $countries)) {
				$key = array_search($form['tcontact__country']['und'][0]['chado-contactprop__value']['#default_value'], $countries);
				$form['tcontact__country']['und'][0]['chado-contactprop__value']['#default_value'] = $key;
			}
		}
		else {
			drupal_set_message('Could not find Country field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
		}
		return $form;
}

/*
Add state list to country field
*/
/**
 * Helper function to state text field into a select list
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_alterStateFieldToSelectList($form) {
		global $us_states, $canada_provinces;
		
		//dpm($form['tcontact__country']['und'][0]);
		if(isset($form['tcontact__state'])) {
			$form['tcontact__state']['und'][0]['chado-contactprop__value']['#type'] = 'select';
			
			//$all_options = array_merge($us_states, $canada_provinces);
			
			//check if the key is mixed up with the value. This happens for old fields in which the full
			//state is the value. This module makes use of 2 or 3 alphabetical values instead (key for $countries)
			$all_options = array_merge($us_states, $canada_provinces);
			//dpm($all_options);
			
			if(in_array($form['tcontact__state']['und'][0]['chado-contactprop__value']['#default_value'], $us_states)) {
				$form['tcontact__state']['und'][0]['chado-contactprop__value']['#options'] = $all_options;
				$key = array_search($form['tcontact__state']['und'][0]['chado-contactprop__value']['#default_value'], $all_options);
				$form['tcontact__state']['und'][0]['chado-contactprop__value']['#default_value'] = $key;
				
				
				//Don't like it but for some reason form State field does not get set, so this does it
				$js = "
					jQuery(document).ready(function() {
						try {
							jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').val('$key');
						}
						catch(err) {
							
						}
					});
				";
				drupal_add_js($js, 'inline');
			}
			elseif(in_array($form['tcontact__state']['und'][0]['chado-contactprop__value']['#default_value'], $canada_provinces)) {
				$form['tcontact__state']['und'][0]['chado-contactprop__value']['#options'] = $all_options;
				$key = array_search($form['tcontact__state']['und'][0]['chado-contactprop__value']['#default_value'], $all_options);
				$form['tcontact__state']['und'][0]['chado-contactprop__value']['#default_value'] = $key;
				
				
				//Don't like it but for some reason form State field does not get set, so this does it
				$js = "
					jQuery(document).ready(function() {
						try {
							jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').val('$key');
						}
						catch(err) {
							
						}
					});
				";
				drupal_add_js($js, 'inline');
			}
			
			//dpm($form['tcontact__state']['und'][0]);
		}
		else {
			drupal_set_message('Could not find State field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
		}
		return $form;
}


/*
This function converts the Organization Field into an autocomplete widget
This function is called from the main tripal_contact_profile_form_alter hook function
*/
/**
 * Helper function to convert organization text field into an autocomplete list
 *
 * @param array $form
 *   The form array
 *
 *
 * @return array
 *  Returns altered form array
 *
 */
function tripal_contact_profile_alterOrganizationField($form) {

	if(isset($form['tcontact__organization'])) {
	$form['tcontact__organization']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
		/*
		if ($form['tcontact__organization']['und'][0]['chado-contactprop__value']['#default_value'] == ' ') { //this is when no data has been set, it is usual a single whitespace from default for this field
			$form['tcontact__organization']['und'][0]['chado-contactprop__value']['#default_value'] = '';
		}*/
		$form['tcontact__organization']['und'][0]['chado-contactprop__value']['#autocomplete_path'] = 'ajax/load/contact/organization';	
	}
	else {
		drupal_set_message('Could not find Country field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}
	return $form;
}

/*
This function converts the Position, Department and City Field into text fields
This function is called from the main tripal_contact_profile_form_alter hook function
*/
function tripal_contact_profile_alterCommonFieldsToTextFields($form) {

	//Position
	if(isset($form['sep__position'])) {
		$form['sep__position']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
	}
	else {
		drupal_set_message('Could not find Position field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}
	
	//Firstname
	if(isset($form['local__firstname'])) {
		$form['local__firstname']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
		//Need to get type_id for population
		$args = array();
		$results = chado_query("SELECT * FROM chado.cvterm JOIN chado.cv ON chado.cvterm.cv_id = chado.cv.cv_id WHERE cvterm.name ILIKE 'FirstName' AND cv.name LIKE 'local'", $args);
		$type_id = ""; //dummy value that is invalid
		foreach ($results as $row) {
			$type_id = $row->cvterm_id;
		}
		$form['local__firstname']['und'][0]['chado-contactprop__type_id']['#value'] = $type_id;
		
	}
	else {
		drupal_set_message('Could not find First Name field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}

	//Lastname
	if(isset($form['local__lastname'])) {
		$form['local__lastname']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
		//Need to get type_id for population
		$args = array();
		$results = chado_query("SELECT * FROM chado.cvterm JOIN chado.cv ON chado.cvterm.cv_id = chado.cv.cv_id WHERE cvterm.name ILIKE 'LastName' AND cv.name LIKE 'local'", $args);
		$type_id = ""; //dummy value that is invalid
		foreach ($results as $row) {
			$type_id = $row->cvterm_id;
		}
		$form['local__lastname']['und'][0]['chado-contactprop__type_id']['#value'] = $type_id;
		
	}
	else {
		drupal_set_message('Could not find Last Name field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}		
	
	//Department
	if(isset($form['tcontact__department'])) {
		$form['tcontact__department']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
		//Need to get type_id for population
		$args = array();
		$results = chado_query("SELECT * FROM chado.cvterm JOIN chado.cv ON chado.cvterm.cv_id = chado.cv.cv_id WHERE cvterm.name ILIKE 'Department' AND cv.name LIKE 'tripal_contact'", $args);
		$type_id = ""; //dummy value that is invalid
		foreach ($results as $row) {
			$type_id = $row->cvterm_id;
		}
		$form['tcontact__department']['und'][0]['chado-contactprop__type_id']['#value'] = $type_id;
		
	}
	else {
		drupal_set_message('Could not find Department field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}	
	
	
	//City
	if(isset($form['tcontact__city'])) {
		$form['tcontact__city']['und'][0]['chado-contactprop__value']['#type'] = 'textfield';
		//Need to get type_id for population
		$args = array();
		$results = chado_query("SELECT * FROM chado.cvterm JOIN chado.cv ON chado.cvterm.cv_id = chado.cv.cv_id WHERE cvterm.name ILIKE 'City' AND cv.name LIKE 'tripal_contact'");
		$type_id = ""; //dummy value that is invalid
		foreach ($results as $row) {
			$type_id = $row->cvterm_id;
		}
		$form['tcontact__city']['und'][0]['chado-contactprop__type_id']['#value'] = $type_id;		
	}
	else {
		drupal_set_message('Could not find City field, usually this happens if you did not click install in the Tripal Contact Profile configuration page or check new fields in the Tripal Contact manage fields sections.', 'warning');
	}		
	return $form;
}


?>