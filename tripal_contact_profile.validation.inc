<?php

/*
This happens right before a form is saved
Think of this like preprocessing the data in arrays and then using it to save to database
*/
/**
 * Implements hook_form_validate().
 */
function tripal_contact_profile_form_validate(&$form, &$form_state) {
	//dpm("VALIDATE");
	
	//check validation for user registration form
	
	if(@$form['#id'] == 'user-register-form') {
		if(variable_get('contact_basic_validation_for_registration_fields_setting', 0) == 1) {
			//drupal_set_message('User Registration extra validation');
			//dpm($form_state['values']);
			if (isset($form_state['values']['mail'])) {
				//mail validation
				$email = @$form_state['values']['mail'];
				//drupal_set_message("Found email: $email");
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					form_set_error('mail', 'You entered a malformed email address, please double check your email address');
				}
			}
			if (isset($form_state['values']['tcp_fullname']['und'][0]['value'])) {
				//mail validation
				$full_name = $form_state['values']['tcp_fullname']['und'][0]['value'];
				preg_match('/(.*[^ ]), (.*[^ ])/', $full_name, $check_1_array);
				preg_match('/(.*[^ ]),(.*[^ ])/', $full_name, $check_2_array);
				
				$check_1_count = count($check_1_array);
				$check_2_count = count($check_2_array);
				//dpm($check_1_array);
				//dpm($check_2_array);
				
				$name_error = true;
				
				if($check_1_count >= 3) {
					if(trim($check_1_array[2]) != '') {
						$name_error = false;
					}
				}
				
				if($check_2_count >= 3) {
					if(trim($check_1_array[2]) != '') {
						$name_error = false;
					}
				}	



				if($name_error == true) {
					form_set_error('tcp_fullname', 'You must enter a valid name in the format:Last, First');
				}

				//drupal_set_message("Full name: $full_name");
				
			}			
		}
	}
	//variable_get('contact_basic_validation_for_registration_fields_setting', 0)
	
	
	$bundle_machine_name = tripal_contact_profile_getMachineName();
	if(@$form['#bundle'] == $bundle_machine_name && $bundle_machine_name != "" && $bundle_machine_name != null && @$form['#id'] == 'tripal-entity-form') {
	
		drupal_set_title('');
		
		if(@arg(1) == 'registermulti') {
			$css2 = "
				#edit-local-email {
					display: none;
				}
			";
			drupal_add_css($css2, 'inline');
			
			
			//cater for other fields CSS
			
			if(isset($_SESSION['form_state_1']['values']['tcp_fullname']['und'][0]['value'])) {
				$css_names = "
					#edit-local-firstname {
						display: none;
					}
					
					#edit-local-lastname {
						display: none;
					}					
				";
				drupal_add_css($css_names, 'inline');
			}		
			
			if(isset($_SESSION['form_state_1']['values']['tcp_organization']['und'][0]['value'])) {	
				$css_organization = "
					#edit-tcontact-organization {
						display: none;
					}
				";
				drupal_add_css($css_organization, 'inline');
				//dpm($organization);
			}

			
		}
		
		//Modify the submission function if it's for user registration, check for the mode REQUEST variable in url
		//check for special GET string called 'mode'
		$mode = filter_xss(check_plain(@$_REQUEST['mode']));
			
		if($mode == "clean") {
			$css = "
				.container {
					margin-left: 0px !important;
					margin-right: 0px !important;
					padding-left: 0px !important;
					padding-right: 0px !important;
					/* width: 97.5% !important; */
				}
				
				.form-item-sep--position-und-0-chado-contactprop--value .help-block {
					display: none;
				}
			";
			drupal_add_css($css, 'inline');
		}
	
		if(variable_get('contact_basic_validation_for_default_tcp_fields_setting', 0) == 1) {
			//check if valid email
			$email = $form_state['values']['local__email']['und'][0]['chado-contactprop__value'];
			//drupal_set_message("Found email: $email");
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				form_set_error('local__email', 'You entered a malformed email address, please double check your email address');
			}

			//check if valid websites
			if(isset($form_state['values']['local__url']['und']['add_more'])) {
				//it's multivalued
				$url_field_count = count($form_state['values']['local__url']['und']) - 1; //remove add more from count
			}
			else {
				$url_field_count = 1;
			}
			for($i=0; $i < $url_field_count; $i++) {
				//dpm($form_state['values']['local__url']['und'][$i]);
					$url_value = @$form_state['values']['local__url']['und'][$i]['url_type'] . " " . @$form_state['values']['local__url']['und'][$i]['url'];
					//this if is a strange one, we need to cater for empty fields to not error out (part of multifields)
					
					if($url_value != ' ') {
						//check if url_type/website_type is set
						if($form_state['values']['local__url']['und'][$i]['url_type'] == '') {
							//this is an error
							form_set_error('local__url_type_' . $i, 'You forgot to select the website type for website ' . ($i + 1));
						}
						//check url format
						// Remove all illegal characters from a url
						$url = $form_state['values']['local__url']['und'][$i]['url'];
						$url = filter_var($url, FILTER_SANITIZE_URL);					
						if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)) {
							//echo("$url is a valid URL");
						} else {
							form_set_error('local__url_url_' . $i, 'The url for website ' . ($i + 1) . ' must have http:// or https:// and must also be a valid url');
						}
					}	

			}
			
		}
		

		
		//Merge First and Last Name to save as Title
		$first_name = $form_state['values']['local__firstname']['und'][0]['chado-contactprop__value'];
		$last_name = $form_state['values']['local__lastname']['und'][0]['chado-contactprop__value'];
		$organization = $form_state['values']['tcontact__organization']['und'][0]['chado-contactprop__value'];
		//dpm($form['schema__name']['und']['#default_value']);
		//dpm($form_state['values']['schema__name']);
		//dpm($form_state['values']['local__firstname']);
		//dpm($form['schema__name']);
		$form_state['values']['schema__name']['und'][0]['value'] = $last_name . ", " . $first_name;
		
		$args = array();
		$typeid_person_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Person' LIMIT 1;", $args);
		$typeid_person = -1;
		foreach($typeid_person_record as $temp_row) {
			$typeid_person = $temp_row->cvterm_id;
		}
		//dpm($form_state['values']['schema__additional_type']['und'][0]);
		$form_state['values']['schema__additional_type']['und'][0]['chado-contact__type_id'] = $typeid_person;
		//drupal_set_message('Type ID set to Person');
		//dpm($form_state['values']['schema__name']);
		
		if(form_get_errors()) {
			return;
		}		
		
		//CATER FOR GENUS FIELD
		//dpm($form_state['values']['local__genus']['und']);
		//debug($form_state, '$form_state', TRUE);
		//dpm($form['local__genus']);
		if(isset($form_state['values']['local__genus']['und']['add_more'])) {
			//it's multivalued
			$genus_field_count = count($form_state['values']['local__genus']['und']) - 1; //remove add more from count
		}
		else {//it's single field
			$genus_field_count = 1;
			//bug detection - some reason, contact_id does not populate
			$form_state['values']['local__genus']['und'][0]['chado-contactprop__contact_id'] = $form_state['values']['local__email']['und'][0]['chado-contactprop__contact_id'];
			$form_state['values']['local__genus']['und'][0]['chado-contactprop__rank']= 0;
		}
		
		//debug($form);
		//dpm($form_state['values']);
		for($i=0; $i < $genus_field_count; $i++) {
			
			    //dpm("IT CAME HERE");


				
				//dpm($form['local__genus']['und'][$i]);
				//dpm($form_state['values']['local__genus']['und'][$i]);
				
				$form_state['values']['local__genus']['und'][$i]['chado-contactprop__value'] = @$form_state['values']['local__genus']['und'][$i]['genus'] . " " . 
																			@$form_state['values']['local__genus']['und'][$i]['species'];
				$form_state['values']['local__genus']['und'][$i]['chado-contactprop__value'] = trim($form_state['values']['local__genus']['und'][$i]['chado-contactprop__value']);
				$form_state['values']['local__genus']['und'][$i]['value'] = $form_state['values']['local__genus']['und'][$i]['chado-contactprop__value'];
				
				//Cater for species form field if revalidating
				//This DOES NOT WORK, THANKS DRUPAL LOL
				/*
				dpm($form_state['values']['local__genus']['und'][$i]);
				dpm($form['local__genus']['und'][$i]['species']);
				if ($form['local__genus']['und'][$i]['species']['#value'] != ""){
					$species_value = $form['local__genus']['und'][$i]['species']['#value'];
					$species_options = array("$species_value" => "$species_value");
					$form['local__genus']['und'][$i]['species']['#options'] = $species_options;
					dpm($form['local__genus']['und'][$i]['species']);
				}
				*/
				
				
				//if it's a multiple value form, we need to add weights
				if(variable_get('contact_genus_convertmultivalue_setting') == 1) {
					$form_state['values']['local__genus']['und'][$i]['chado-contactprop__rank'] = @$form_state['values']['local__genus']['und'][$i]['_weight'];
				}
				//get cvtermid of genus
				$genus_cvterm_id = tripal_contact_profile_get_cvtermid('local', 'genus');
				$form_state['values']['local__genus']['und'][$i]['chado-contactprop__type_id'] = $genus_cvterm_id; 
				

				//This is if the field is empty, delete it from the database
				//dpm($form_state['values']['local__genus']['und'][$i]);
				if(@$form_state['values']['local__genus']['und'][$i]['genus'] == "" && @$form_state['values']['local__genus']['und'][$i]['species'] == "") {
					@chado_query('DELETE FROM chado.contactprop WHERE contactprop_id = ' . intval($form_state['values']['local__genus']['und'][$i]['chado-contactprop__contactprop_id']) . ';', array());
					unset($form_state['values']['local__genus']['und'][$i]);
					unset($form['local__genus']['und'][$i]);
				}
			//dpm($form_state['values']['local__genus']['und'][$i]);
		}
		//dpm($form_state['values']['local__genus']['und']);
		
		
		//CATER FOR URL FIELD
		//local__url
		if(isset($form_state['values']['local__url']['und']['add_more'])) {
			//it's multivalued
			$url_field_count = count($form_state['values']['local__url']['und']) - 1; //remove add more from count
		}
		else {//it's single field
			$url_field_count = 1;
			//bug detection - some reason, contact_id does not populate
			$form_state['values']['local__url']['und'][0]['chado-contactprop__contact_id'] = $form_state['values']['local__email']['und'][0]['chado-contactprop__contact_id'];
			$form_state['values']['local__url']['und'][0]['chado-contactprop__rank']= 0;
		}
		
		//debug($form);
		//dpm($form_state['values']);
		for($i=0; $i < $url_field_count; $i++) {
			//dpm($form_state['values']['local__url']['und'][$i]);
				$form_state['values']['local__url']['und'][$i]['chado-contactprop__value'] = @$form_state['values']['local__url']['und'][$i]['url_type'] . " " . 
																			@$form_state['values']['local__url']['und'][$i]['url'];
				$form_state['values']['local__url']['und'][$i]['chado-contactprop__value'] = trim($form_state['values']['local__url']['und'][$i]['chado-contactprop__value']);

				//if it's a multiple value form, we need to add weights
				if(variable_get('contact_url_convertmultivalue_setting') == 1) {
					$form_state['values']['local__url']['und'][$i]['chado-contactprop__rank'] = @$form_state['values']['local__url']['und'][$i]['_weight'];
				}
				//get cvtermid of url
				$url_cvterm_id = tripal_contact_profile_get_cvtermid('local', 'url');
				$form_state['values']['local__url']['und'][$i]['chado-contactprop__type_id'] = $url_cvterm_id; 
				

				//This is if the field is empty, delete it from the database
				//dpm($form_state['values']['local__url']['und'][$i]);
				if(@$form_state['values']['local__url']['und'][$i]['url_type'] == "" && @$form_state['values']['local__url']['und'][$i]['url'] == "") {
					@chado_query('DELETE FROM chado.contactprop WHERE contactprop_id = ' . intval($form_state['values']['local__url']['und'][$i]['chado-contactprop__contactprop_id']) . ';', array());
					unset($form_state['values']['local__url']['und'][$i]);
					unset($form['local__url']['und'][$i]);
				}
		}
		
		//CATER FOR TOPIC FIELD
		//local__researchinterest
		if(isset($form_state['values']['local__researchinterest']['und']['add_more'])) {
			//it's multivalued
			$researchinterest_field_count = count($form_state['values']['local__researchinterest']['und']) - 1; //remove add more from count
		}
		else {//it's single field
			$researchinterest_field_count = 1;
			//bug detection - some reason, contact_id does not populate
			$form_state['values']['local__researchinterest']['und'][0]['chado-contactprop__contact_id'] = $form_state['values']['local__email']['und'][0]['chado-contactprop__contact_id'];
			$form_state['values']['local__researchinterest']['und'][0]['chado-contactprop__rank']= 0;
		}
		
		//debug($form);
		//dpm($form_state['values']);
		for($i=0; $i < $researchinterest_field_count; $i++) {
				//dpm($form_state['values']['local__researchinterest']['und'][$i]);
				//$form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__value'] = $form_state['values']['topic__url']['und'][$i]['url_type'] . " " . 
				//															$form_state['values']['local__url']['und'][$i]['url'];
				$form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__value'] = trim($form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__value']);

				//if it's a multiple value form, we need to add weights
				if(variable_get('contact_researchinterest_convertmultivalue_setting') == 1) {
					$form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__rank'] = $form_state['values']['local__researchinterest']['und'][$i]['_weight'];
				}
				//get cvtermid of topic
				$topic_cvterm_id = tripal_contact_profile_get_cvtermid('local', 'researchinterest');
				$form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__type_id'] = $topic_cvterm_id; 
				

				//This is if the field is empty, delete it from the database
				if($form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__value'] == "") {
					@chado_query('DELETE FROM chado.contactprop WHERE contactprop_id = ' . intval($form_state['values']['local__researchinterest']['und'][$i]['chado-contactprop__contactprop_id']) . ';', array());
					unset($form_state['values']['local__researchinterest']['und'][$i]);
					unset($form['local__researchinterest']['und'][$i]);
				}
		}
		
		//check if no form errors and in edit mode, check for linked user account to modify tcp_fullname if it exists
		//if (arg(2) == 'edit' && !form_get_errors()) {
		if (arg(2) == 'edit' && !form_get_errors()) {	
			//lookup user based on email
			//try to change tcp_fullname
			if(isset($form_state['values']['local__email']['und'][0]['chado-contactprop__value'])) {
				$email = trim($form_state['values']['local__email']['und'][0]['chado-contactprop__value']);
				//dpm($email);
				$user_account = user_load_by_mail($email);
				if($user_account !== FALSE) {
					//dpm($user_account);
					$edit = array(
					  'tcp_fullname' => array(
						'und' => array(
						  0 => array(
							'value' => $last_name . ", " . $first_name,
						  ),
						),
					  ),
					  'tcp_organization' => array(
						'und' => array(
						  0 => array(
							'value' => $organization,
						  ),
						),
					  ),					  
					);
					user_save($user_account, $edit);
					//drupal_set_message("Awesome! We've also synced your first and last name into your account's full name field.");
				}
			}
			

		}
		
	}
	//dpm($form_state['values']);
}
?>