Developer
==================================================
This section gives some insight into understanding how TCP works under the hood. We welcome anyone willing to 
submit updates or bug fixes (where applicable).

==================================================
+ Module Files
==================================================
This section explains the different files that constitute to the Tripal Contact Profile module.

==================================================
File: tripal_contact_profile.info
==================================================
Purpose Summary: Typical info file. REQUIRED BY DRUPAL 7.

This file contains Drupal related markup to make a module appear in the admin module list.

==================================================
File: tripal_contact_profile.module
==================================================
Purpose Summary: Mostly bootstrapping by including other files and hook_menu implementation. REQUIRED BY DRUPAL 7.

This file contains the main code for the module. Drupal needs this file in order to start running the module.
We use this file primarily to include other files needed by the TCP module thus you will find include_once 
functions near to the top of this file. The intention is to keep this  file to do mostly bootstrap related 
functions for the module. Most notably, the hook_menu for creating drupal related url pages 
for the module can be found here.

==================================================
File: tripal_contact_profile.admin.inc
==================================================
Purpose Summary: Primarily used to create the TCP Administration UI pages.

This file contains functions which generate the TCP Administration UI pages. These are the pages found at 
/admin/tripal/tripal_contact_profile. These functions are executed via the hook_menu (tripal_contact_profile_menu)
implementation found in tripal_contact_profile.module
 
==================================================
File: tripal_contact_profile.ajax.inc
==================================================
Purpose Summary: Primarily used to create ajax related functions that respond with JSON answers.

This file contains functions which generate AJAX pages that usually result with JSON answers. 
Most of the functions in here are either used on the actual TCP entity edit forms OR the user account registration 
form. Some functions include checking to see whether an account already exists with an email, retrieving species list 
when a genus is queried and more. 

==================================================
File: tripal_contact_profile.canada_provinces_array.inc
==================================================
Purpose Summary: Created a global array containing Canada Provinces.

This file is included once using the include_once function in the tripal_contact_profile.module. It 
essentially provides an array that contains all Canadian provinces which is used by the module for the 
States / Province field. 

==================================================
File: tripal_contact_profile.us_states_array.inc
==================================================
Purpose Summary: Created a global array containing all US states.

This file is included once using the include_once function in the tripal_contact_profile.module. It 
essentially provides an array that contains all US states which is used by the module for the 
States / Province field. 

==================================================
File: tripal_contact_profile.form_alter.inc
==================================================
Purpose Summary: Primarily used to alter forms using the hook_alter_form API. 

This file is one of the most important files within the TCP module. It uses the hook_form_alter drupal api 
to alter most of the forms in which it is assigned to alter. This is primarily used to alter the TCP edit form 
and also the user register form most to convert text fields into more useful fields like select lists etc.

==================================================
File: tripal_contact_profile.form_alter_subfunctions.inc
==================================================
Purpose Summary: Primarily contains functions called by form_alter.inc to simplify code understanding. 

This file is also one of the most important files within the TCP module. To simplify the form_alter.inc code 
sub functions were created inside this file so converting text fields into more useful types of fields like 
select lists etc.

==================================================
File: tripal_contact_profile.functions.inc
==================================================
Purpose Summary: Primarily contains useful functions to get Tripal entity information. 

This file contains useful functions and is used to retrieve Tripal entity information.


==================================================
File: tripal_contact_profile.linker.inc
==================================================
Purpose Summary: Linker functions used to generate the /editprofile link as well as messages on the /user
pages to edit or create Tripal Contact Profiles. 


==================================================
File: tripal_contact_profile.registermulti.inc
==================================================
Purpose Summary: Used to generate the multi-step registration form that replaces the original drupal user 
registration form. 

This file contains all the code used to create multi step / multi page form users during a new user 
registration. It can replace the /user/register original form with this multi-step form essentially allowing 
a user to register for the original Drupal account but then move over to step 2 which registers a TCP profile.
It submits both the user registration and the TCP profile which is much more convenient.

==================================================
File: tripal_contact_profile.validation.inc
==================================================
Purpose Summary: Used primarily to hold validation functions for the TCP entity form.

This file contains the hook_form_validate api function used to ensure that data entered in the form passes 
the correct format or returns errors to the user to enter the correct formats.










