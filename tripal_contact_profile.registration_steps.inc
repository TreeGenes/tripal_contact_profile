<?php

/**
 * @file
 * DEPRECATED Registration Steps (Ctools Wizard) integration for the Tripal Contact Profile module.
 */
 
/*
THIS CODE IS DEPRECATED. IT WAS USED TO TEST WHETHER CTOOLS WIZARD WOULD BE
APPROPRIATE FOR MULTISTEP REGISTRATION - BUT THERE WAS AN EASIER WAY TO GET THIS
DONE. DON'T RELY ON THIS PIECE OF CODE.
*/

function tripal_contact_profile_registration_ctools_wizard($step = 'register') {
	ctools_include('wizard');
	ctools_include('object-cache');
	
	$form_info = array(
		'id' => 'multistep_registration',
		'path' => "user/multiregister/%step",
		'show trail' => TRUE,
		'show back' => FALSE,
		'show return' => FALSE,
		'next callback' => 'tripal_contact_profile_registration_subtask_next',
		'finish callback' => 'tripal_contact_profile_registration_subtask_finish',
		'return callback' => 'tripal_contact_profile_registration_subtask_finish',
		'cancel callback' => 'tripal_contact_profile_registration_subtask_cancel',
		'order' => array(
			'register' => t('Register'),
			'tcp' => t('Details')
		),
		'forms' => array(
			'register' => array(
				'form id' => 'user_register_form'
			),
			'tcp' => array(
				'form id' => 'tripal_entity_form',
				'term id' => tripal_contact_profile_getMachineName_id(),
			),
		),
	);
	
	$form_state['signup_object'] = tripal_contact_profile_registration_get_page_cache('signup');
	
	$output = ctools_wizard_multistep_form($form_info, $step, $form_state);
	
	return $output;
}

/**
* Callback executed when the 'next' button is clicked.
*/
function tripal_contact_profile_registration_subtask_next(&$form_state) {
	// Store submitted data in a ctools cache object, namespaced 'signup'.
	tripal_contact_profile_registration_set_page_cache('signup', $form_state['values']);
}

/**
* Callback executed when the 'cancel' button is clicked.
*/
function tripal_contact_profile_registration_subtask_cancel(&$form_state) {
	// Clear our ctools cache object. It's good housekeeping.
	tripal_contact_profile_registration_clear_page_cache('signup');
}

/**
* Callback executed when the entire form submission is finished.
*/
function tripal_contact_profile_registration_subtask_finish(&$form_state) {
	// Clear our Ctool cache object.
	tripal_contact_profile_registration_clear_page_cache('signup');
	 
	// Redirect the user to the front page.
	drupal_goto('<front>');
}


function tripal_contact_profile_registration_get_page_cache($name) {
	ctools_include('object-cache');
	$cache = ctools_object_cache_get('tripal_contact_profile_registration', $name);
	 
	// If the cached object doesn't exist yet, create an empty object.
	if (!$cache) {
		$cache = new stdClass();
		$cache->locked = ctools_object_cache_test('tripal_contact_profile_registration', $name);
	}
	 
	return $cache;
}

function tripal_contact_profile_registration_set_page_cache($name, $data) {
	ctools_include('object-cache');
	$cache = ctools_object_cache_set('tripal_contact_profile_registration', $name, $data);
}

function tripal_contact_profile_registration_clear_page_cache($name) {
	ctools_include('object-cache');
	ctools_object_cache_clear('tripal_contact_profile_registration', $name);
}
?>