[![Tripal Rating Silver Status](https://tripal.readthedocs.io/en/7.x-3.x/_images/Tripal-Silver.png)](https://tripal.readthedocs.io/en/7.x-3.x/extensions/module_rating.html#Silver)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3463310.svg)](https://doi.org/10.5281/zenodo.3463310)
# Latest documentation or scroll below to view quick install instructions
https://tripal-contact-profile.readthedocs.io/en/latest/

# Important
* To get the best experience, this module works well with the Colleagues Directory module to create a Community!

# Install Instructions
* Make sure you have the latest version of Tripal (3.x with Chado v1.3) installed
* Upload files in this git repository into your sites modules directory preferrably in a subdirectory named 'tripal_contact_profile'
* Enable the module
* Go to Tripal -> Tripal Contact Profile Configuration
* Click Install
* Make sure to add permissions to create a Tripal Content Type in admin -> users -> permissions.
* Required, tweak the settings in the Tripal Contact Profile Configuration using the upper tabs 'Field Options' and 'Linker options' and make sure to click save. Even if you didn't tweak settings!
* Optionally, go to Structure -> Tripal Content Types -> Tripal Contact Profile -> Manage Fields to adjust the order of the fields


# What is Tripal Contact Profile?
This module creates a Tripal Content Type called Tripal Contact Profile.
Tripal Contact Profile holds standard fields in which a Tripal user can create their own 'profile'
It was created for the TreeGenes project as the base content type for making a
'Colleagues Directory' where users could display their information to others.

# Features
* Automatic creation of Tripal Content Type - 'Tripal Contact Profile'
* Automatic creation of fields within the Tripal Contact Profile tripal content type
* Field options which replace standard input form elements into specialized / more user friendly elements
* Linker options which adds 'Edit profile' link and default Drupal /user menu items
* Easily differentiate and form the submit/edit form with the class '.tripal_contact_profile_form' using CSS.
* Migrate 'Contact' to 'Tripal Contact Profile'
* Alter contactprop genus integers to organism/species text values

# Technical
Tripal Default Fields(Name, Description)
|-- All additional sub fields

# Migration (Technical)
* Changed to automated functions!

# Changelog
2018-08-20
A lot of migration features added to make it easier to move from 'Contact' to 'Tripal Contact Profile'

2018-08-15
Added field option to remove descriptions
Added root class 'tripal_contact_profile_form'

2018-08-14
Replaced Topic with a local Research Interest field due to clashing on TG website.

2018-08-13
Switched the Tripal Content Type name to Tripal Contact Profile due to clashes with original Contact tripal content type.
Fixed Genus/Species select lists to hold values properly.

2018-07-25
Contact login linker added
Contact login linker admin options
Edit profile menu item option
Contact profile tab in user/* option
Cache clearing button
Convert Topic from single value to multi-value
Convert Genus from single value to multi-value


2018-07-24
ORCID ajax import field integration
Organization autocomplete field integration
Autodetect email field integration
Hide description field option
Initial features added to administration section

2018-07-20
Country select list field integration

2018-07-19
Added CVTERM 'Email address'

2018-07-18
Added some Contact properties programmatically

2018-07-17
Added install clause

2018-07-16
Added admin page