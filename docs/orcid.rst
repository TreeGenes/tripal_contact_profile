ORCID Integration
====================================
We are pleased to let you know that Tripal Contact Profile
integrates AJAX importing of ORCID data. The feature is enabled
by default and integrates directly with the ORCID field found
on the create/edit Tripal Contact Profile form.

The ORCID data retrieved from orcid.org in which each field data
in the array that has been programmed currently into our release
checks to see whether the data is empty or not. If the data is not
empty, that data is imported into the Tripal Contact Profile form.
