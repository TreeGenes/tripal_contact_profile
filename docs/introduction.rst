Introduction
==================================================

===============================
Overview
===============================
This module creates a tripal contact type labelled Tripal Contact Profile within a Tripal powered website.
Tripal Contact Profile is a tripal contact type that holds standard fields in which a Tripal user can create their own contact 'profile'
but adds additional features to make the process of submitting user data more simple. Please see features section for further details.
The module utilizes underlying Tripal and Drupal API, making it compatible with tripal core and views module enabling you to create a profile directory for example using Views. (or you can use
the supplemental module `Colleagues Directory <https://gitlab.com/TreeGenes/colleagues_directory>`_ which creates a simple community automatically.).
The module was created for the TreeGenes project as the base content type 
for making a 'Colleagues Directory' where users could display their information to others.

========
Features
========
* Automatic creation of tripal contact type 'Tripal Contact Profile'
* Automatic creation of standard fields within the Tripal Contact Profile tripal content type
* Migrate old 'Contact' to 'Tripal Contact Profile'
* Alter contactprop genus integers to organism/species text values
* Field options which replace standard input form elements into specialized / more user friendly elements
* Linker options which adds 'Edit profile' link to standard 'User' menu
* Easily differentiate and form the submit/edit form with the class '.tripal_contact_profile_form' using CSS.
