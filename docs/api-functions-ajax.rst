Functions - AJAX
====================================

**tripal_contact_profile.ajax.inc**

* *tripal_contact_profile_load_organization_autocomplete_ajax($text)*
   ``Helper function to get organization listing from the database via ajax``
   ``$text will be used as a partial or full string for searching the organization name records in contactprops``
   ``Function outputs drupal JSON data so it cannot be used as an include but rather as a menu link in Drupal``
* *tripal_contact_profile_load_orcid_ajax($orcid)*
   ``Helper function to get ORCID data from ORCID website via ajax``
   ``$orcid is the ORCID that will be used to query ORCID website for details``
   ``Function outputs drupal JSON data so it cannot be used as an include but rather as a menu link in Drupal``
