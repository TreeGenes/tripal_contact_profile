<?php
/**
 * @file
 * RegisterMulti integration for the Tripal Contact Profile module.
 */

//STEP 1 - User Registration page
/**
 * This function outputs step 1 - User Registration page
 *
 *
 * @return string
 *  Returns page content in html
 *
 */
function tripal_contact_profile_register_multi_step1_form() {
	$form = drupal_get_form('user_register_form');
	



	//dpm($form);
	
	//print drupal_render($form);
	

	
	$markup = '';
	$markup .= drupal_render($form);
	$content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $markup,
	);

	return $content;	
}

//STEP 1 - User Registration handle submit
/**
 * This function saves step 1 submit data
 *
 *
 * @return null
 *  Does not return anything, used to redirect to page
 *
 */
function tripal_contact_profile_register_multi_step1_submitpage(&$form, &$form_state) {
	//save the data into session variables for later processing
	
	//this caters for if name is missing / this is to cater for how TreeGenes and Mimubase works and keep it operational for all sites
	if(!isset($form_state['values']['name'])) {
		$form_state['values']['name'] = $form_state['values']['mail'];
	}
	else {
		//check if name exists and if email contains @fs.fed.us (special needs for TreeGenes)
		$results = chado_query(
			'SELECT * FROM chado.contact WHERE name ILIKE :name LIMIT 1',
			array(
				':name' => $form_state['values']['tcp_fullname']['und'][0]['value']
			)
		);
		//dpm('FULLNAME:' . $form_state['values']['tcp_fullname']['und'][0]['value']);
		$contact_id = -1;
		//dpm($results);
		foreach($results as $row) {
			$contact_id = $row->contact_id;
		}
		//dpm('Contact ID detected:' . $contact_id);
		$results = chado_query(
			"SELECT * FROM chado.contactprop WHERE contact_id = :contact_id AND value ILIKE :email_domain LIMIT 1",
			array(
				':contact_id' => $contact_id,
				':email_domain' => '%@fs.fed.us%'
			)
		);
		foreach($results as $row) {
			//we need to delete this contact and also entity
			//dpm('DELETING CONTACT_ID:' . $contact_id);
			
			//delete the entity
			$machine_name = tripal_contact_profile_getMachineName();
			//dpm('Machine Name:' . $machine_name);
			$entity_records = chado_query(
				'SELECT * FROM chado_' . $machine_name .' WHERE record_id = :record_id LIMIT 1', 
				array(
					':record_id' => $contact_id
				)
			);
			foreach($entity_records as $entity_record) {
				//dpm('Entity ID Found so deleting it:' . $entity_record->entity_id);
				entity_delete('TripalEntity', $entity_record->entity_id);
			}
			
			//delete the contact in chado
			
			chado_query(
				'DELETE FROM chado.contact WHERE contact_id = :contact_id;', 
				array(
					':contact_id' => $contact_id
				)
			);
			
			
			//delete the contactprop in chado
			
			chado_query(
				'DELETE FROM chado.contactprop WHERE contact_id = :contact_id;', 
				array(
					':contact_id' => $contact_id
				)
			);	
			
			
		}		
		

	}
	

	$_SESSION['form_1'] = $form;
	$_SESSION['form_state_1'] = $form_state;
	
	//dpm(array_keys($_SESSION['form_state_1']));
	//dpm($_SESSION['form_state_1']['values']);
	//dpm('Session data set 1');

	if(@$_SESSION['form_state_1']['values']['registermulti_study_trees'] == 0) { //No
		//skip step 2 and go to finish
		drupal_goto('user/registermulti/finish');
	}
	else {
		drupal_goto('user/registermulti/step2');
	}

}

//STEP 2 - Generate TCP form
/**
 * This function generates step 2 TCP registration form
 *
 *
 * @return string
 *  Returns html of the tcp registration form
 *
 */
function tripal_contact_profile_register_multi_step2_form() {
	//dpm($_SESSION['form_state_1']);

	$form = drupal_get_form("tripal_entity_form", tripal_contact_profile_getMachineName_id());
	//dpm($form);
	//dpm(array_keys($form));
	unset($form['cancel_button']);
	//print drupal_render($form);
	
	//Override the submit function for the user_register_form (user_register_submit)
	//unset($form['#submit']);
	//$form['#submit'][] = 'tripal_contact_profile_register_multi_step2_submit';
	drupal_set_title('Details');

	$markup = '';
	$markup .= drupal_render($form);
	$content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $markup,
	);

	return $content;	
}

//STEP 2 - Handle TCP form submission
/**
 * This function saves submit data from tcp registration form
 *
 *
 * @return null
 *  Does not return anything, used to redirect to page
 *
 */
function tripal_contact_profile_register_multi_step2_submitpage(&$form, &$form_state) {
	//save the data into session variables for later processing
	//$_SESSION['form_2'] = $form;
	//$_SESSION['form_state_2'] = $form_state;
	
	ctools_include('object-cache');
	ctools_object_cache_set('registermulti', 'form_2', $form);
	ctools_object_cache_set('registermulti', 'form_state_2', $form_state);
	
	
	//dpm('Session data set 2');
	drupal_goto('user/registermulti/finish');
	
	//dpm($_SESSION);
}

//STEP 3 - Finish by submitting the data to the necessary submission functions, detect for any submission errors
/**
 * This function saves submit data to database
 *
 *
 * @return string
 *  Returns html of submission response
 *
 */
function tripal_contact_profile_register_multi_finishpage() {
	$markup = '';
	/*
			ctools_include('object-cache');
			$form_2 = ctools_object_cache_get('registermulti', 'form_2');	
			$form_state_2 = ctools_object_cache_get('registermulti', 'form_state_2');
			dpm($form_state_2['values']);
	*/
	
	
	//dpm($_SESSION['form_1']);
	//dpm($_SESSION['form_state_1']);
	
	
	$success_1 = false;
	try {
		$form_1 = $_SESSION['form_1'];
		$form_state_1 = $_SESSION['form_state_1'];
		user_register_submit($form_1, $form_state_1);
		$success_1 = true;
	}
	catch (Exception $e) {
		$markup .= 'Caught exception: ' .  $e->getMessage() . "<br />";
	}
	
	$success_2 = false;
	if($success_1 == true) {
		try {
			ctools_include('object-cache');
			$form_2 = ctools_object_cache_get('registermulti', 'form_2');	
			$form_state_2 = ctools_object_cache_get('registermulti', 'form_state_2');
			//dpm($form_state_2['TripalEntity']);
			tripal_entity_form_submit($form_2, $form_state_2);
			$success_2 = true;
		}
		catch (Exception $e) {
			$markup .= 'Caught exception: ' .  $e->getMessage() . "<br />";
		}	
	}
	
	
	//dpm($_SESSION['form_state_2']);
	
	if($success_1 && $success_2) {
		$markup .= "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-ok.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-ok.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-ok.png' /> Finished</div></div>";
		$markup .= "Thank you for your registration! Both your account and your account details have been successfully submitted!<br />";
		unset($_SESSION['form_1']);
		unset($_SESSION['form_state_1']);
		ctools_include('object-cache');
		ctools_object_cache_clear('registermulti', 'form_2');	
		ctools_object_cache_clear('registermulti', 'form_state_2');		
	}
	elseif ($success_1 && !$success_2) {
		$markup .= "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-ok.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-error.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-ok.png' /> Finished</div></div>";
		$markup .= "Partial success: Your user account has been created but your further details was not created! Don't worry, once you login, you can continue creating your full profile.<br />";
	}
	elseif (!$success_1) {
		$markup .= "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-ok.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-ok.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-error.png' /> Finished</div></div>";
		$markup .= "Unsuccessful: Sorry but your user account could not be created and thus your full details profile was also not created. Please restart from the <a href='user/registermulti/step1'>beginning</a>.<br />";
	}
	$content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $markup,
	);

	return $content;	
}

?>