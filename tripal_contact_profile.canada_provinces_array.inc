<?php
/**
 * @file
 * Canada provinces integration for the Tripal Contact Profile module.
 */

global $canada_provinces;
$canada_provinces = array(
	"- Canada Provinces -" => "- Canada Provinces -",
	"Not Available" => "Not Available",
	"Alberta" => "Alberta",
	"British Columbia" => "British Columbia",
	"Manitoba" => "Manitoba",
	"New Brunswick" => "New Brunswick",
	"Newfoundland and Labrador" => "Newfoundland and Labrador",
	"Northwest Territories" => "Northwest Territories",
	"Nova Scotia" => "Nova Scotia",
	"Nunavut" => "Nunavut",
	"Ontario" => "Ontario",
	"Prince Edward Island" => "Prince Edward Island",
	"Quebec" => "Quebec",
	"Saskatchewan" => "Saskatchewan",
	"Yukon"	=> "Yukon"	
); 
 
?>