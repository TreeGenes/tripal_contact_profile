Migration
==================================================
Tripal Contact Profile module offers a method of migrating old 'Contact' nodes
into the new Tripal Contact Profile nodes. This process will create the necessary
entities within Tripal so that users can edit their profile via the default
Tripal Contact Profile edit form.

Migrating will automatically create records as well as change the type_ids
associated with the old 'Contact' nodes. This means you will no longer be able
to use the 'Contact' content type to edit contacts and should instead use the 
Tripal Contact Profile content type to make changes.

To perform a migration
   1. Login as Administrator.
   2. Go to Administration page.
   3. Click 'Tripal'.
   4. Click 'Tripal Contact Profile Configuration'.
   5. Make sure you already used the 'Install' button to automatically create the Tripal Contact Profile content type.
   6. Click 'Migrate All Old Contacts To Tripal Contact Profile'.

You should receive the necessary notifications once completed.

IMPORTANT: In order for PHP to note time out during a large migration, please ensure that you temporarily 
extend the php execution time. For approximately 3000 records, we used 5 minutes as the timeout.