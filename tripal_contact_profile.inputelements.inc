<?php
/**
 * @file
 * DEPRECATED InputElements integration for the Tripal Contact Profile module.
 */

/*
THIS IS DEPRECATED, THE CODE MAY NOT WORK
DO NOT RELY ON THESE FUNCTIONS
THEY WERE TESTED BUT THE FIELDS ADD ARRAYS WHICH ARE NOT COMPATIBLE WITH CONTACTPROP_VALUES

Author       : Risharde Ramnath
Description  : This file contains custom input field elements. Particularly for Genus 
               which accepts a genus select list and a species select list
Help Resource: https://www.silviogutierrez.com/blog/custom-drupal-elements-forms/
*/

/*
This hook gives Drupal info about our custom elements
*/
function tripal_contact_profile_element_info() {
	//drupal_set_message("Creating Genus Dual Select Field");
	$elements['genus_dual_select_field'] = array(
		'#default_value' => '',
		'#input' => TRUE,
		'#process' => array('tripal_contact_profile_genus_dual_select_field_element_process'),
		'#theme' => array('genus_dual_select_field'),
		'#theme_wrappers' => array('form_element'),
		'#tree' => TRUE,
		'#value_callback' => 'tripal_contact_profile_genus_dual_select_field_element_value_callback',
	);
	return $elements;
}

/*
Genus Dual Select Field - Process Function
This basically creates the sub elements that make up this Genus Dual Select Field (one field)
*/
function tripal_contact_profile_genus_dual_select_field_element_process($element, $form_state, $complete_form) {
	//Sub element Genus (select list)
	$element['genus_dual_select_field']['genus'] = array(
		'#type' => 'select',
		'#input' => true,
		'#empty_option' => '- ' . t('Genus') . ' -',
		'#options' => array(
		  'github' => 'github://',
		  'bitbucket' => 'bitbucket://',
		),
		'#required' => $element['#required'],
		'#title' => t('Genus'),
		'#title_display' => 'invisible',
		'#theme_wrappers' => array(),
	);

	
	if (isset($element['#default_value']['genus_dual_select_field']['genus'])) {
		$element['genus_dual_select_field']['genus']['#default_value'] = $element['#default_value']['genus_dual_select_field']['genus'];
	}
	

	//Sub element Species (select list)
	$element['genus_dual_select_field']['species'] = array(
		'#type' => 'select',
		'#input' => true,
		'#empty_option' => '- ' . t('Species') . ' -',
		'#options' => array(
		  'github' => 'github://',
		  'bitbucket' => 'bitbucket://',
		),
		'#required' => $element['#required'],
		'#title' => t('Species'),
		'#title_display' => 'invisible',
		'#theme_wrappers' => array(),
	);
	


	
	if (isset($element['#default_value']['genus_dual_select_field']['species'])) {
		$element['genus_dual_select_field']['species']['#default_value'] = $element['#default_value']['genus_dual_select_field']['species'];
	}
	
	
	
	
	//dpm($element);

	

	//drupal_set_message('Element creation');
	
	return $element;
}

/*
Theming of the elements
Used to actually REGISTER the render of the Genus Dual Select Field
*/
function tripal_contact_profile_theme() {
	return array(
		'genus_dual_select_field' => array(
			'render element' => 'element',
		),
	);
}

/*
Used to actually render the Genus Dual Select Field in HTML
Apparently the tripal_contact_profile_theme() hook
needs the following custom function and 
Drupal documentation on this is missing?
*/
function theme_genus_dual_select_field($variables) {
	$element = $variables['element'];
	$output = '';
	$output .= drupal_render($element['genus_dual_select_field']['genus']);
	$output .= " "; // This space forces our fields to have a little room in between.
	$output .= drupal_render($element['genus_dual_select_field']['species']);

	//dpm($output);
	return $output;
}


/*
This caters for submission of the values entered.
If there is one part selected and the other not, the entire field value is set to empty
*/
function tripal_contact_profile_genus_dual_select_field_element_value_callback($element, $input = FALSE, &$form_state) {

	if ($input !== FALSE) {
		//if genus has data, but none for species, reset genus
		
		if ($input['genus_dual_select_field']['genus'] && !$input['genus_dual_select_field']['species']) {
			$input['genus_dual_select_field']['genus'] = '';
		}
		
		//if species has entered data, but none for genus, reset species
		if ($input['genus_dual_select_field']['species'] && !$input['genus_dual_select_field']['genus']) {
			$input['genus_dual_select_field']['species'] = '';
		}
		
		
		return $input;
	}
	elseif (!empty($element['#default_value'])) {

		return $element['#default_value'];
	}
	return;
}

function tripal_contact_profile_chado_query_alter(&$sql, &$args) {
	dpm($sql);
	dpm($args);
}
?>