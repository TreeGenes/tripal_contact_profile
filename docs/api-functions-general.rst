Functions - General
====================================

**tripal_contact_profile.functions.inc**

* *tripal_contact_profile_get_contact_entity_id()*
   ``Helper function to get current user Tripal Contact Profile Entity number``
* *tripal_contact_profile_get_contact_entity_id_from_contact_id($contact_id)*
   ``Helper function to get Tripal Contact Profile Entity number from contact_id``
* *tripal_contact_profile_getMachineName()*
   ``This helper function is a highly convenient function that tries to find the actualy machine_name of the bundle from it's human readable label``
* *tripal_contact_profile_getMachineName_id()*
   ``This helper function is a highly convenient function that tries to find the actualy machine_name ID number of the bundle from it's human readable label``
* *tripal_contact_profile_get_cvtermid($cv_name, $cvterm_name)*
   ``This is a helper function used to get the cvterm_id by using the cv_name and cvterm_name``