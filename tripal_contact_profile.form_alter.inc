<?php


/*
This function gets called right before a form loads
*/
/**
 * Implements hook_form_alter().
 */
function tripal_contact_profile_form_alter(&$form, &$form_state, $form_id) {
	//dpm($form_id);
	//dpm($form);
	//dpm('Form Alter');

	//This will alter user registration form
	if($form_id == 'user_register_form') {
		
		//cater for extra user registration validation (email and tcp_fullname)
		//drupal_set_message('Modified user registration validation');
		$form['#validate'][] = 'tripal_contact_profile_form_validate';
		
		$site_name = variable_get('site_name', "");
		if($site_name != "") {
			drupal_set_title($site_name . " User Account");
		}
		else {
			drupal_set_title("User Account");
		}
		
		//if switch is on in Field Options
		if(variable_get('contact_add_tcp_to_registration_form_setting', 0) == 1) {
			$iframe_height = "350";
			
			
			//add onchange to user registration email field and change value in iframe email field and do ajax check to see if there's a record
			$js = "
				jQuery(document).ready(
					function() {
						//console.log('on change TCP full name field, add for iframe first name and last name');
						if(jQuery('#edit-tcp-fullname-und-0-value').length) {
							var iframe = jQuery('#tcp_registration_frame');
							//TCP fullname field exists, which is good
							jQuery('#edit-tcp-fullname-und-0-value').keyup(
								function() {
									if(jQuery(iframe).contents().find('#edit-local-firstname-und-0-chado-contactprop-value').length && jQuery(iframe).contents().find('#edit-local-lastname-und-0-chado-contactprop-value').length)  {
										//console.log('TCP Full Name:' + jQuery('#edit-tcp-fullname-und-0-value').val());
										var tcp_fullname = jQuery('#edit-tcp-fullname-und-0-value').val();
										var tcp_fullname_parts = tcp_fullname.split(',');
										//console.log('TCP FULLNAME PARTS:' + tcp_fullname_parts.length);
										if(tcp_fullname_parts.length > 1) {
											jQuery(iframe).contents().find('#edit-local-firstname-und-0-chado-contactprop-value').val(tcp_fullname_parts[1].trim());
											document.getElementById('tcp_registration_frame').contentWindow.jQuery('#edit-local-firstname-und-0-chado-contactprop-value').trigger('input');
											jQuery(iframe).contents().find('#edit-local-lastname-und-0-chado-contactprop-value').val(tcp_fullname_parts[0].trim());
											document.getElementById('tcp_registration_frame').contentWindow.jQuery('#edit-local-lastname-und-0-chado-contactprop-value').trigger('input');
											
										}
										
									}
								}
							);
						}
						
						//console.log('onchange email field');
						jQuery('#edit-mail').keyup(
							function() {
								var email_val = jQuery('#edit-mail').val();
								if(email_val.includes('@') && email_val.includes('.')) {
									console.log('email_val:' + email_val);
									//check if there's already a TCP contact record, if so, hide step 2
									var hasRecord = false;
									
									jQuery.getJSON('/ajax/load/hastcprecord/' + email_val, function( data ) {
										jQuery.each( data, function( key, val ) {
											console.log(key + ':' + val);
											if(val == true) {
												hasRecord = true;
												jQuery('#tcp_registration_frame').hide();
												jQuery('#registration_step2_notice').html('Great! Your registration just got easier since a detailed profile already exists for your email! Just continue below to finish up your registration.');
											}
											else {
												jQuery('#registration_step2_notice').html('Important: Please ensure that you click the \'SAVE\' button in this subsection!');
												jQuery('#tcp_registration_frame').show();
												iframetop();
											}
										});
										//console.log('hasRecord:' + hasRecord);
									});	
									
									
									//check is user registration email exists, if it does, disable submit button, show message to login
									jQuery.getJSON('/ajax/load/user_account_exists/' + email_val, function( data ) {
										jQuery.each( data, function( key, val ) {
											console.log(key + ':' + val);
											if(val == true) {
												jQuery('#edit-submit').hide();
												//console.log(jQuery('#edit-submit').parent().html());
												if( jQuery('#account_exist_status').length ) {
													jQuery('#account_exist_status').show();
												}
												else {
													//create this element
													jQuery('<p id=\'account_exist_status\'></p>').insertBefore(jQuery('#user-register-form').parent());
													jQuery('#account_exist_status').html('An account with this email address already exists, please <a href=\'/user/login\'>login here</a>');
												}
												jQuery('#user-register-form').hide();
											}
											else {
												jQuery('#edit-submit').show();
												if( jQuery('#account_exist_status').length ) {
													jQuery('#account_exist_status').hide();	
												}
												//jQuery('#registration_step2_notice').html('Important: Please ensure that you click the \'SAVE\' button in this subsection!');
												//jQuery('#tcp_registration_frame').show();
												//iframetop();
											}
										});
										//console.log('hasRecord:' + hasRecord);
									});										
									
									
									//console.log('detected change:' + jQuery('#edit-mail').val());
									var iframe = jQuery('#tcp_registration_frame');
									jQuery(iframe).contents().find('#edit-local-email-und-0-chado-contactprop-value').val(jQuery('#edit-mail').val());
								}
							}
						);
					}
				);
			";
			drupal_add_js($js, 'inline');
			
			//The following function is used to scroll to the top of the iframe and hide the header
			$js = "
				var iframe = '';
				function iframetop() {
						console.log('Frame loaded');
						try {
							//console.log(jQuery('#tcp_registration_frame'));
							iframe = jQuery('#tcp_registration_frame');
							console.log(jQuery('#edit-local-orcid-und-0-chado-contactprop-value', iframe.contents()).position());
							
							//jQuery(iframe).contents().find('html, body)
							jQuery(iframe).contents().find('html').animate({ scrollTop: (($iframe_height/2) - 0) + jQuery('#tripal-entity-form', iframe.contents()).position().top}, { duration: 'medium', easing: 'swing' });
							

							console.log('Done');
							
							
						}
						catch(err) {
							
						}
						
						//hide header
						try {
							jQuery(iframe).contents().find('header').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}
						try {
							jQuery(iframe).contents().find('#header').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}	
						try {
							jQuery(iframe).contents().find('#page-title').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}	
						try {
							jQuery(iframe).contents().find('footer').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}	
						try {
							jQuery(iframe).contents().find('#footer').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}
						
						
						try {
							jQuery(iframe).contents().find('#edit-local-email').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}
						try {
							jQuery(iframe).contents().find('#edit-local-firstname').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}
						try {
							jQuery(iframe).contents().find('#edit-local-lastname').css('display', 'none');
						}
						catch(err) {
							console.log(err);
						}						
						

						try{
						setIframeHeight('tcp_registration_frame');	
						}
						catch(err) {
							console.log(err);
						}
						
						jQuery(iframe).contents().find('#edit-tcontact-country-und-0-chado-contactprop-value').change(
							function() {
								try{
									setIframeHeight('tcp_registration_frame');
									setTimeout(function() {
										console.log('setTimeout');
										//jQuery(document).animate({ scrollTop: (($iframe_height/2) - 0) + jQuery('#edit-tcontact-country-und-0-chado-contactprop-value', iframe.contents()).position().top}, { duration: 'medium', easing: 'swing' });
										jQuery(document).find('html').animate({ scrollTop: (($iframe_height)  - 0) + jQuery('#edit-tcontact-country-und-0-chado-contactprop-value', iframe.contents()).position().top}, { duration: 1, easing: 'linear' });
									},100);
								}
								catch(err) {
									console.log(err);
								}
							}
						);
						
				}
				
				//This was obtained from https://www.dyn-web.com/tutorials/iframes/height/
				function getDocHeight(doc) {
					doc = doc || document;
					// stackoverflow.com/questions/1145850/
					var body = doc.body, html = doc.documentElement;
					var height = Math.max( body.scrollHeight, body.offsetHeight, 
						html.clientHeight, html.scrollHeight, html.offsetHeight );
					return height;
				}				
				
				//This was obtained from https://www.dyn-web.com/tutorials/iframes/height/
				function setIframeHeight(id) {
					var ifrm = document.getElementById(id);
					var doc = ifrm.contentDocument? ifrm.contentDocument: 
						ifrm.contentWindow.document;
					ifrm.style.visibility = 'hidden';
					ifrm.style.height = '10px'; // reset to minimal height ...
					// IE opt. for bing/msn needs a bit added or scrollbar appears
					ifrm.style.height = getDocHeight( doc ) + 10 + 'px';
					ifrm.style.visibility = 'visible';
				}				

				//jQuery('#edit-tcontact-state').show();
				//jQuery()

				
				
			";
			drupal_add_js($js, 'inline');
			
			//dpm($form);
			//This tries to hook into the submit button and adds some html code to allow users to submit additional details in Tripal Contact Profile
			//@$form['account']['mail']['#prefix'] .= "<h2>Basic Information</h2><br />";
			//@$form['actions']['submit']['#prefix'] .= "<br /><h2>User Account Details</h2>";
			global $base_url;
			$form['actions']['submit']['#prefix'] .= "<iframe id='tcp_registration_frame' onload='iframetop();' width='100%' height='" . $iframe_height . "px' frameborder='0' src='/bio_data/add/" . tripal_contact_profile_getMachineName_id() . "?mode=clean'></iframe><br /><br />";
			$form['actions']['submit']['#prefix'] .= "<h2>Complete User Account Registration</h2><h5 id='registration_step3_notice'><img src='$base_url/misc/message-16-info.png'> Please make sure you clicked the SAVE button above if one exists under the Further Details section, then click the button below to create an account.</h5><br />";
		}
		
		//dpm(arg(1));
		
		if(arg(1) == 'registermulti') {
			//Override the submit function for the user_register_form (user_register_submit)
			//unset($form['#validate']);
			$js = "
				jQuery(document).ready(
					function() {
						

						
						//console.log('onchange email field');
						jQuery('#edit-mail').keyup(
							function() {
								var email_val = jQuery('#edit-mail').val();
								if(email_val.includes('@') && email_val.includes('.')) {
									console.log('email_val:' + email_val);
									//check if there's already a TCP contact record, if so, hide step 2
									var hasRecord = false;
									
									jQuery.getJSON('/ajax/load/hastcprecord/' + email_val, function( data ) {
										jQuery.each( data, function( key, val ) {
											console.log(key + ':' + val);
											if(val == true) {
												hasRecord = true;
												//jQuery('#tcp_registration_frame').hide();
												//jQuery('#registration_step2_notice').html('Great! Your registration just got easier since a detailed profile already exists for your email! Just continue below to finish up your registration.');
											}
											else {
												//jQuery('#registration_step2_notice').html('Important: Please ensure that you click the \'SAVE\' button in this subsection!');
												//jQuery('#tcp_registration_frame').show();
												//iframetop();
											}
										});
										//console.log('hasRecord:' + hasRecord);
									});	
									
									
									//check is user registration email exists, if it does, disable submit button, show message to login
									jQuery.getJSON('/ajax/load/user_account_exists/' + email_val, function( data ) {
										jQuery.each( data, function( key, val ) {
											console.log(key + ':' + val);
											if(val == true) {
												jQuery('#edit-submit').hide();
												//console.log(jQuery('#edit-submit').parent().html());
												if( jQuery('#account_exist_status').length ) {
													jQuery('#account_exist_status').show();
												}
												else {
													//create this element
													jQuery('<p id=\'account_exist_status\'></p>').insertBefore(jQuery('#user-register-form').parent());
													jQuery('#account_exist_status').html('An account with this email address already exists, please <a href=\'/user/login\'>login here</a>');
												}
												jQuery('#user-register-form').hide();
											}
											else {
												jQuery('#edit-submit').show();
												if( jQuery('#account_exist_status').length ) {
													jQuery('#account_exist_status').hide();	
												}
												//jQuery('#registration_step2_notice').html('Important: Please ensure that you click the \'SAVE\' button in this subsection!');
												//jQuery('#tcp_registration_frame').show();
												//iframetop();
											}
										});
										//console.log('hasRecord:' + hasRecord);
									});										
									
									
									//console.log('detected change:' + jQuery('#edit-mail').val());
									//var iframe = jQuery('#tcp_registration_frame');
									//jQuery(iframe).contents().find('#edit-local-email-und-0-chado-contactprop-value').val(jQuery('#edit-mail').val());
								}
							}
						);
					}
				);
			";
			drupal_add_js($js, 'inline');						
			
			unset($form['#submit']);
			$form['#submit'][] = 'tripal_contact_profile_register_multi_step1_submitpage';
			if(variable_get('registermulti_study_trees', 0) == 1 && variable_get('contact_redirect_register_to_registermulti', 0) == 1) {
				//add form item to choose if interested in studying trees
				$form['registermulti_study_trees'] = array(
					'#type' => 'select',
					'#title' => 'Are you interested in studying trees?',
					'#options' => array(
						1 => t('Yes'),				
						0 => t('No'),
					 ),
					 '#weight' => 99,
					 '#default_value' => 1,
					 '#required' => true,
					 //'#description' => t('Are you interested in studying trees?'),
				);
			}

			$form['actions']['submit']['#value'] = 'Continue to Step 2';
			if(!isset($form['#prefix'])) {
				$form['#prefix'] = "";
			}
			/*
			if(isset($_SESSION['form_state_1'])) {
				$form_state = $_SESSION['form_state_1'];
				$form['#prefix'] = "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-ok.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-error.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-error.png' /> Finished</div></div>";
			}
			else {
				$form['#prefix'] = "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-help.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-error.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-error.png' /> Finished</div></div>";
			}
			*/
			$form['#prefix'] = "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-help.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-error.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-error.png' /> Finished</div></div>";
		}

	}
	
	//This will alter the Tripal Contact Profile TripalEntity Form
	$bundle_machine_name = tripal_contact_profile_getMachineName();
	if(@$form['#bundle'] == $bundle_machine_name && $bundle_machine_name != "" && $bundle_machine_name != null && @$form['#id'] == 'tripal-entity-form') {
		
		drupal_set_title('');

		//Force email to be a required field
		$form['local__email']['und'][0]['chado-contactprop__value']['#required'] = TRUE;	
		
		if(variable_get('contact_remove_descriptions_setting', 0)) {
			$css = "<style>
				.description {
					display: none !important;
				}
			</style>";
			$form['#prefix'] = $form['#prefix'] . $css;
		}
		
		//dpm($form['#validate']);
		
		//Perform additional validations
		$form['#validate'][] = 'tripal_contact_profile_form_validate';
		$form['#attributes']['class'][] = 'tripal_contact_profile_form';
		//dpm($form['#attributes']);
		
		/* Initialize a temporary variable for modified form */
		$form_temp = null;
		
		//Alter Titles for Organization -> Institution
		//$form['tcontact__organization']['und'][0]['chado-contactprop__value']['#title'] = "Institution";
		
		//Alter Titles for First Name and Last Name fields
		$form['local__firstname']['und'][0]['chado-contactprop__value']['#title'] = "First Name";
		$form['local__lastname']['und'][0]['chado-contactprop__value']['#title'] = "Last Name";
		//dpm($form['schema__name']['und'][0]['value']['#default_value']);
		
		//If schema name already has value, populate first name and last name (this is here
		//to make sure old records that only have schema__name will populate the new records
		$name_parts = explode(', ', $form['schema__name']['und'][0]['value']['#default_value']);
		$form['local__firstname']['und'][0]['chado-contactprop__value']['#default_value'] = @$name_parts[1];
		$form['local__lastname']['und'][0]['chado-contactprop__value']['#default_value'] = @$name_parts[0];
		
		if(isset($form['schema__additional_type']['und'][0]['chado-contact__type_id'])) {
			
			$args = array();
			$typeid_person_record = chado_query("SELECT cvterm_id, * FROM chado.cvterm WHERE name LIKE 'Person' LIMIT 1;", $args);
			$typeid_person = -1;
			foreach($typeid_person_record as $temp_row) {
				$typeid_person = $temp_row->cvterm_id;
			}				
			$form['schema__additional_type']['und'][0]['chado-contact__type_id']['#default_value'] = $typeid_person;
			//dpm($form['schema__additional_type']['und'][0]['chado-contact__type_id']);
		}
		
		$form = tripal_contact_profile_alterCommonFieldsToTextFields($form);	
		
		if(variable_get('contact_populate_from_user_registration_setting') == 1) {
			$form = tripal_contact_profile_populateFromUserRegistration($form);
		}
		
		/*
		Check settings to see if Organization Field should be an autocomplete
		widget instead of a text field
		*/
		//dpm(variable_get('contact_organization_autocomplete_setting'));
		
		if(variable_get('contact_position_select_setting') == 1) {
			$form = tripal_contact_profile_alterPositionFieldToSelectList($form);
		} 
		
		if(variable_get('contact_organization_autocomplete_setting') == 1) {
			$form = tripal_contact_profile_alterOrganizationField($form);
		}
		
		
		if(variable_get('contact_country_select_setting') == 1) {
			$form = tripal_contact_profile_alterCountryFieldToSelectList($form);
		}
		
		if(variable_get('contact_state_select_setting') == 1) {
			$form = tripal_contact_profile_alterStateFieldToSelectList($form);
		}		
		
		if(variable_get('contact_email_autodetect_setting') == 1) {
			$form = tripal_contact_profile_autodetectEmail($form);
		}
		
		if(variable_get('contact_orcid_autopopulate_setting') == 1) {
			$form = tripal_contact_profile_orcid_autopopulate($form);
		}
		
		if(variable_get('contact_description_hide_setting') == 1) {
			$form = tripal_contact_profile_description_hide($form);
		}
		
		
		if(variable_get('contact_genus_converttoselectlists_setting') == 1) {
			tripal_contact_profile_genus_convert_to_selectlists($form, $form_state);
		}
		
		if(variable_get('contact_url_convert_to_compoundfield_setting') == 1) {
			tripal_contact_profile_url_convert_to_compoundfield($form, $form_state);
		}
		
		

		
		if(variable_get('contact_researchinterest_convertmultivalue_setting') == 1) {
			//Disable AJAX for Add more button, it just messes up the form items
			//Unfortunately, I haven't found a way to hook into the AJAX function
			//Every time AJAX is executed, the fields get reset to normal text fields
			//and in this case, we don't want that to happen.			
			unset($form['local__researchinterest']['und']['add_more']['#ajax']);
			
			
			//always +1 fields since Drupal/Chado adds an extra field to add new data
			$researchinterest_field_count = $form_state['field']['local__researchinterest']['und']['items_count'] + 1; 
		}
		else {
			//if it's a single value
			$researchinterest_field_count = 1;
		}
		//Go through each topic field
		$form['local__researchinterest']['und']['#title'] = variable_get('contact_researchinterest_label_setting', 'Research Interests');
		for($i=0; $i < $researchinterest_field_count; $i++) {
			//dpm($form['local__researchinterest']['und'][$i]);
			$form['local__researchinterest']['und'][$i]['chado-contactprop__value']['#title'] = '';
		}
		//dpm($form['local__genus']);
		
		//Modify the submission function if it's for user registration, check for the mode REQUEST variable in url
		//check for special GET string called 'mode'
		$mode = filter_xss(check_plain(@$_REQUEST['mode']));
		
		$css = "
			/*
			.container {
				margin-left: 0px !important;
				margin-right: 0px !important;
				padding-left: 0px !important;
				padding-right: 0px !important;
				/* width: 97.5% !important; */
			}
			*/
			
			.form-item-sep--position-und-0-chado-contactprop--value .help-block {
				display: none;
			}
			
			.form-item-sep--position-und-0-chado-contactprop--value .description {
				display: none;
			}			
		";	
		if($mode == "clean") {
			$form['#submit'][] = 'tripal_contact_profile_submitform_redirect_to_thankyou_page';

			drupal_add_css($css, 'inline');
		}	

		$css2 = "

			
			#edit-local-email {
				display: none;
			}
		
		";
		if(arg(1) == 'registermulti') {
			//Override the submit function for the user_register_form (user_register_submit)
			//unset($form['#validate']);
			//dpm($form['#submit']);
			unset($form['#submit']);
			
			$form['#submit'][] = 'tripal_contact_profile_register_multi_step2_submitpage';
			//dpm(array_keys($form));
			//dpm($form['add_button']);
			$form['add_button']['#value'] = 'Complete Registration';
			//dpm($form);
			
			/*
			if(isset($_SESSION['form_state_2'])) {
				$form_state = $_SESSION['form_state_2'];
			}
			*/
			
			
			ctools_include('object-cache');
			//ctools_object_cache_get('registermulti', 'form_2');
			
			$form_state_2 = ctools_object_cache_get('registermulti', 'form_state_2');
			if(isset($form_state_2)) {
				//$form_state = $form_state_2; //this causes an autosubmit
				//$form['#prefix'] = "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-ok.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-ok.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-error.png' /> Finished</div></div>";
			}
			else {
				
			}
			$form['#prefix'] = "<div id='registermulti_step_status'><div id='step_status_1'><img src='/misc/message-16-ok.png' /> Step 1 - User Account</div><div id='step_status_2'><img src='/misc/message-16-error.png' /> Step 2 - Details</div><div id='step_status_3'><img src='/misc/message-16-error.png' /> Finished</div></div>";
			
			//dpm($_SESSION['form_state_1']['values']);
			
			
			if(isset($_SESSION['form_state_1']['values']['tcp_fullname']['und'][0]['value'])) {
				$full_name = $_SESSION['form_state_1']['values']['tcp_fullname']['und'][0]['value'];
				$full_name_parts = explode(',',$full_name);
				//dpm($full_name_parts);
				$last_name = $full_name_parts[0];
				$first_name = $full_name_parts[1];
				
				//set it into the new form_state for TCP
				//dpm(array_keys($form['local__firstname']['und'][0]));
				//dpm(array_keys($form));
				$form['local__firstname']['und'][0]['chado-contactprop__value']['#default_value'] = trim($first_name);
				$form['local__lastname']['und'][0]['chado-contactprop__value']['#default_value'] = trim($last_name);
				//dpm($form['schema__name']['und'][0]);
				$form['schema__name']['und'][0]['value']['#default_value'] = $full_name;
				
				$css_names = "
					#edit-local-firstname {
						display: none;
					}
					
					#edit-local-lastname {
						display: none;
					}					
				";
				drupal_add_css($css_names, 'inline');
				
			}
			
			if(isset($_SESSION['form_state_1']['values']['tcp_country']['und'][0]['value'])) {
				$country = $_SESSION['form_state_1']['values']['tcp_country']['und'][0]['value'];
				
				//set it into the new form_state for TCP
				//dpm(array_keys($form['local__firstname']['und'][0]));
				//dpm(array_keys($form));
				$form['tcontact__country']['und'][0]['chado-contactprop__value']['#default_value'] = $country;
				$form['tcontact__country']['und'][0]['value']['#value'] = $country;
				$form['tcontact__country']['und'][0]['#instance']['default_value'] = $country;	
				//dpm($form['tcontact__country']);
			}

			if(isset($_SESSION['form_state_1']['values']['tcp_organization']['und'][0]['value'])) {
				$organization = $_SESSION['form_state_1']['values']['tcp_organization']['und'][0]['value'];
				$form['tcontact__organization']['und'][0]['chado-contactprop__value']['#default_value'] = trim($organization);
				
				$css_organization = "
					#edit-tcontact-organization {
						display: none;
					}
				";
				drupal_add_css($css_organization, 'inline');
				//dpm($organization);
			}
			
			if(isset($_SESSION['form_state_1']['values']['mail'])) {
				$email = $_SESSION['form_state_1']['values']['mail'];
				$form['local__email']['und'][0]['chado-contactprop__value']['#default_value'] = $email;
			}
			
			drupal_add_css($css, 'inline');
			drupal_add_css($css2, 'inline');
			//dpm($form_state['values']);
			
		}		
		//dpm($form);
	}
	
}


?>