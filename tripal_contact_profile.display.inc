<?php
/**
 * @file
 * DEPRECATED Display integration for the Tripal Contact Profile module.
 */

/*
Special thanks to Sean Beuhler for discovering awesome Drupal code to create tables
which was altered in this case to display contacts
DEPRECATED BUT MAY BE USED LATER ON DEPENDING ON NEED
*/
function tripal_contact_profile_contacts_directory_page() {
	global $base_url;
	$markup = "";
	
	$results_data_header = array(
		array(
			'data' => 'ID',
			'field' => 'contact_id',
		),
		array(
			'data' => 'Name',
			'field' => 'name',
			'sort' => 'ASC', // Only set default sort order for 1 field
		),
		/*
		array(
		'data' => 'Start Position (Query)',
		'field' => 'start_position_query',
		),
		array(
		'data' => 'End Position (Query)',
		'field' => 'end_position_query',
		),
		*/
	);
	
	$select = db_select('chado.contact','t')
			->extend('PagerDefault')
			->extend('TableSort');
	$select->condition('contact_id',0,'>')
			->fields('t',array('contact_id','name'))
			->limit(15)
			->orderByHeader($results_data_header);
	$results_data = $select->execute();
	
	
	$machine_name = tripal_contact_profile_getMachineName();
	
	foreach ($results_data as $row) {
		$results_data_rows[] = array(
			$row->contact_id,
			"<a href='$base_url/bio_data/" . tripal_contact_profile_get_contact_entity_id_from_contact_id($row->contact_id) .  "'>" . $row->name . '</a>',
		);
	}
	$results_attributes = array(
		'class' => array(
			'resultsTable',
		),
	);
	$results_table_vars = array(
		'header' => $results_data_header,
		'rows'   => $results_data_rows,
		'attributes'    => $results_attributes
	);
	$output = theme('table',$results_table_vars);
	$output .= theme('pager');
	
	$markup .= $output;
	
	$content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $markup,
	);
	return $content;
}


?>