<?php
/***********
Tripal Contact Profile linker functions basically display a notice with links to Tripal Contact Profile 
if one exists for the user OR tells them that they should create one

This is where the magic happens. Basically it checks to see if the url of the user is /user/*
since during login, the user is sent usually to the /user/uid page. So let's make that where
the user sees what's happening.
************/
/**
 * Implements hook_page_alter().
 */
function tripal_contact_profile_page_alter(&$page) {
	
	$path = current_path();
	
	
	//if tcp_registration on user_registration (?mode=basic)
	if(drupal_match_path($path, 'bio_data/add/' . tripal_contact_profile_id())) {
		
		//check for special GET string called 'mode'
		$mode = filter_xss(check_plain(@$_REQUEST['mode']));
		
		if($mode == "clean") {
			//dpm("Detected Tripal Contact Profile Submission Page");
			//dpm($page);
			foreach ($page as $key => $val) {
				//dpm($key);
			}
			unset($page['page_top']);
			unset($page['sidebar_first']);
			unset($page['sidebar_second']);
			unset($page['footer_first']);
			unset($page['footer_second']);
			unset($page['footer_fourth']);
			unset($page['bottom_right']);
			unset($page['footer_third']);
			unset($page['footer']);
			unset($page['header']);
		}
		//dpm($page['#post_render']);
		//$page['sidebar_second'] = null;
	}
	
	//if tcp_registration thankyou page user_registration (?mode=basic)
	if(drupal_match_path($path, 'tripal_contact_profile/thankyou')) {
		
		//check for special GET string called 'mode'
		$mode = filter_xss(check_plain(@$_REQUEST['mode']));
		
		if($mode == "clean") {
			
			//dpm($page);
			foreach ($page as $key => $val) {
				//dpm($key);
			}
			unset($page['page_top']);
			unset($page['sidebar_first']);
			unset($page['sidebar_second']);
			unset($page['footer_first']);
			unset($page['footer_second']);
			unset($page['footer_fourth']);
			unset($page['bottom_right']);
			unset($page['footer_third']);
			unset($page['footer']);
			unset($page['header']);
		}
		//dpm($page['#post_render']);
		//$page['sidebar_second'] = null;
	}	
	
	if (variable_get('contact_loginlinker_userprofilenotice_setting') == 1) {
		$module_name = basename(__FILE__, '.module');
		global $user;
		global $base_url;
		
		
		//dpm($path);
		if(drupal_match_path($path, 'user/*') || drupal_match_path($path, 'user') || drupal_get_path_alias($path)=='user/welcome') {
			//drupal_set_message("HOOK PAGE ALTER - tripal_contact_profile_login_linker");

			//dpm($user);
			$uid = $user->uid;
			if($uid > 0) {
				$user_email = @strtolower($user->mail);
				//$user_email = 'gregor.aas@uni-bayreuth.de'; //THIS IS FOR TESTING ONLY(Aas, Gregor)
				
				
				//We need to query chado contacts to see if there's a record that contains this email, we then get the contact_id
				$args = array(
					':user_email' => $user_email,
				);
				$where_clause = "CONTACTPROP.contact_id = CONTACT.contact_id AND CVTERM.cvterm_id = CONTACTPROP.type_id AND  
				CVTERM.name ILIKE 'email' AND CVTERM.definition ILIKE 'Email address' AND CONTACTPROP.value ILIKE :user_email ";
				$sql = "SELECT CONTACT.contact_id AS contact_id_1, CONTACT.name as contact_name, CONTACTPROP.*, CVTERM.* 
					FROM chado.contact AS CONTACT, chado.contactprop AS CONTACTPROP, chado.cvterm AS CVTERM 
					WHERE $where_clause LIMIT 1;";
				//dpm($sql);
				
				$result = chado_query($sql, $args);
				//dpm($result->rowCount());
				
				/* Check whether there is a record found from the query that contains the email address */
				if($result->rowCount() == 1) {
					foreach($result as $r) {//for each is only to iterate, since there is only 1 record, this doesn't pose a problem
						//dpm($r);
						
						/* We now need to get the entity_id from the Drupal table chado_ tripal_contact_profile_getMachineName() */
						$args = array(
							':contact_id_1' => $r->contact_id_1,
						);
						$result_entity_id = chado_query("SELECT entity_id FROM chado_" . tripal_contact_profile_getMachineName() . " WHERE record_id=:contact_id_1 LIMIT 1;", $args);
						foreach($result_entity_id as $r_entity_id) {
							//dpm($r_entity_id);
							$entity_id = $r_entity_id->entity_id;
							
							/* 
							   Now it's time to change ownership of the entity_id if uid=1, set it to the user's uid which is in $uid
							   For the sake of knowing whether an entity is not connected to the right user id, let's do a query
							*/
							
							$result_unconnected = chado_query("SELECT * FROM tripal_entity WHERE id=" . $entity_id . " AND uid=1 LIMIT 1", $args);
							
							if($result_unconnected->rowCount() > 0) {
								//We found a record that hasn't been tied to the user, so let's set the uid to $uid
								$args = array(
									':uid' => $uid,
									':entity_id' => $entity_id,
								);
								chado_query("UPDATE tripal_entity SET uid=:uid WHERE id=:entity_id", $args);
								//Now make sure to show this on the account page with the necessary links
								drupal_set_message("Your contact details were found and is now imported into your user account.<br />
								<a href='" . $base_url .  "/bio_data/$entity_id/edit'>Edit details</a> - <a href='" . $base_url .  "/bio_data/$entity_id'>View details</a>");
							}
							else{
								//Record is tied in, show the account page message with necessary links
								drupal_set_message("Your contact details are connected to your user account.");
								drupal_set_message("Please use the following form to update or modify your profile details.  These details are viewable to the general public.<br />
								<a href='" . $base_url .  "/bio_data/$entity_id/edit'>Edit details</a> - <a href='" . $base_url .  "/bio_data/$entity_id'>View details</a>");
							}
						}	
					}
				}
				else {
					//No record found in chado contact tables, so record exists, let the user know in the account pages and links to create details
					drupal_set_message("No contact details have been found for your account, please [ <a href='" . $base_url. "/bio_data/add/" . tripal_contact_profile_getMachineName_id() . "'>add your contact details here</a> ]", 'warning');
				}
			
			}
		}
	}
}

/*
Create tab page in user/* pages in the top menu
*/
/**
 * Helper function to create loginlinker tab page
 *
 *
 *
 * @return string
 *  Returns html output of page
 *
 */
function tripal_contact_profile_loginlinker_tab_page() {
		$markup = "";
	
		$module_name = basename(__FILE__, '.module');
		global $user;
		//dpm($user);
		global $base_url;
		
		$path = current_path();
		//dpm($path);
		if(drupal_match_path($path, 'user/*') || drupal_match_path($path, 'user') || drupal_get_path_alias($path)=='user/welcome') {
			//drupal_set_message("HOOK PAGE ALTER - tripal_contact_profile_login_linker");

			//dpm($user);
			
			$user_profile = user_load(arg(1));
			$uid = $user_profile->uid;
			//dpm($user_profile);
			
			if($uid > 0) {
				
				/*
				$is_admin = in_array("administrator", $user->roles);
				$user_to_edit = user_load(arg(1));
				dpm($user_to_edit);
				*/
				
				$user_email = @strtolower($user_profile->mail);
				//$user_email = 'gregor.aas@uni-bayreuth.de'; //THIS IS FOR TESTING ONLY(Aas, Gregor)
				
				
				//We need to query chado contacts to see if there's a record that contains this email, we then get the contact_id
				$args = array(
					':user_email' => $user_email,
				);
				$where_clause = "CONTACTPROP.contact_id = CONTACT.contact_id AND CVTERM.cvterm_id = CONTACTPROP.type_id AND  
				CVTERM.name ILIKE 'email' AND CVTERM.definition ILIKE 'Email address' AND CONTACTPROP.value ILIKE :user_email ";
				$sql = "SELECT CONTACT.contact_id AS contact_id_1, CONTACT.name as contact_name, CONTACTPROP.*, CVTERM.* 
					FROM {contact} AS CONTACT, {contactprop} AS CONTACTPROP, {cvterm} AS CVTERM 
					WHERE $where_clause LIMIT 1;";
				//dpm($sql);
				
				$result = chado_query($sql, $args);
				//dpm($result->rowCount());
				
				/* Check whether there is a record found from the query that contains the email address */
				if($result->rowCount() == 1) {
					foreach($result as $r) {//for each is only to iterate, since there is only 1 record, this doesn't pose a problem
						//dpm($r);
						
						/* We now need to get the entity_id from the Drupal table chado_ tripal_contact_profile_getMachineName() */
						$args = array(
							':contact_id_1' => $r->contact_id_1,
						);
						$result_entity_id = chado_query("SELECT entity_id FROM chado_" . tripal_contact_profile_getMachineName() . " WHERE record_id=:contact_id_1 LIMIT 1;", $args);
						foreach($result_entity_id as $r_entity_id) {
							//dpm($r_entity_id);
							$entity_id = $r_entity_id->entity_id;
							
							/* 
							   Now it's time to change ownership of the entity_id if uid=1, set it to the user's uid which is in $uid
							   For the sake of knowing whether an entity is not connected to the right user id, let's do a query
							*/
							$args = array(
								':entity_id' => $entity_id,
							);
							$result_unconnected = chado_query("SELECT * FROM tripal_entity WHERE id=:entity_id AND uid=1 LIMIT 1", $args);
							
							if($result_unconnected->rowCount() > 0) {
								//We found a record that hasn't been tied to the user, so let's set the uid to $uid
								$args = array(
									':entity_id' => $entity_id,
								);
								chado_query("UPDATE tripal_entity SET uid=" . $uid . " WHERE id=:entity_id", $args);
								//Now make sure to show this on the account page with the necessary links
								$markup .= "Contact details were found and is now imported into user account.<br />
								<a href='" . $base_url .  "/bio_data/$entity_id/edit'>Edit details</a> - <a href='" . $base_url .  "/bio_data/$entity_id'>View details</a>";
							}
							else{
								//Record is tied in, show the account page message with necessary links
								$markup .= "Contact details are connected to user account.<br />";
								$markup .= "These details are viewable to the general public.<br />";
								if($user->uid == $user_profile->uid) {
									$markup .= "<a href='" . $base_url .  "/bio_data/$entity_id/edit'>Edit details</a> - ";
								}
								$markup .= "<a href='" . $base_url .  "/bio_data/$entity_id'>View details</a>";
							}
						}	
					}
				}
				else {
					//No record found in chado contact tables, so record exists, let the user know in the account pages and links to create details
					$markup .= "No contact details have been found for your account, please [ <a href='" . $base_url. "/bio_data/add/" . tripal_contact_profile_getMachineName_id() . "'>add your contact details here</a> ]";
				}
			
			}
		}
		
	$content['raw_markup'] = array(
		'#type' => 'markup',
		'#markup' => $markup,
	);

 
  return $content;
}

/*
Generate a redirection page/link for Edit Profile User Menu item 
*/
/**
 * Helper function to generate the Edit Profile Link
 *
 *
 * @return null
 *  Returns nothing but does a page redirect
 *
 */
function tripal_contact_profile_generate_edit_profile_link() {
	$contact_entity_id = tripal_contact_profile_get_contact_entity_id();
	global $user;
	global $base_url;
	$uid = $user->uid;
	if ($contact_entity_id <= 0) {
		header('Location: ' . $base_url . '/bio_data/add/' . tripal_contact_profile_getMachineName_id());
		exit;
	}
	else {
		header('Location: ' . $base_url . '/bio_data/' . $contact_entity_id .'/edit');
		exit;
	}
}
?>