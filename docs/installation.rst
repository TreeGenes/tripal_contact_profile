Installation
==================================================

================================
Requirements
================================
The following requirements were used in our development environment and 
is therefore the recommended requirements for Tripal Contact Profile
to function properly.

* A website running Tripal version 3.x with Chado version 1.3 installed
* The latest Tripal Contact Profile module

=========================
Instructions
=========================
* Make sure you have the latest version of Tripal (3.x with Chado v1.3) installed
* Upload files in this git repository into your sites modules directory preferrably in a subdirectory named 'tripal_contact_profile'
* Enable the module
* Go to Tripal -> Tripal Contact Profile Configuration
* Click Install
* Make sure to add permissions to create a Tripal Content Type in admin -> users -> permissions.
* Required, tweak the settings in the Tripal Contact Profile Configuration using the upper tabs 'Field Options' and 'Linker options' and make sure to click save. Even if you didn't tweak settings!
* Optionally, go to Structure -> Tripal Content Types -> Tripal Contact Profile -> Manage Fields to adjust the order of the fields

