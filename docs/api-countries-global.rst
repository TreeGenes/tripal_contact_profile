Global - Countries
====================================
Tripal Contact Profile module creates a global variable $countries
which contains a list of countries in the world as an array.
The array key is the same as the value and was adapted from an open source list.

**tripal_contact_profile.countries_array.inc**

``global $countries;``

Global - Countries2
====================================
Tripal Contact Profile module creates a global variable $countries2
which contains a list of countries in the world as an array.
The array key is usually 2 or 3 letters long while value is the full name of the country adapted from an open source list.


``global $countries2;`