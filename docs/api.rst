API
==================================================
Tripal Contact Profile module contains API code in which other modules can utilize.
We refer to this as the API which includes helper functions that you can call as long
as the module is enabled.

.. toctree::
   :maxdepth: 3
   :caption: API

   api-functions-general
   api-functions-ajax
   api-countries-global