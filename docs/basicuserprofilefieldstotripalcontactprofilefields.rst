Basic User Profile Fields to Tripal Contact Profile Fields
==========================================================

================================
Basic Registration Fields
================================
Tripal Contact Profile contains a new feature that can create basic fields such as name, organization and country.
This was initially created for TreeGenes so that we can manually determine whether created users are spam users or not.
To create these fields, go to admin -> Tripal -> Tripal Contact Profile Configuration
Make sure you are under the tab 'Install / Uninstall' and scroll down to 'Create Basic Registration Fields' and click on it.

======================================
Autopopulate to Tripal Contact Profile
======================================
After creating basic registration fields which will require new users to enter data, we want to make it easier for a created user
to now be able to create a Tripal Contact Profile which has 'overlapping' fields similar to the registration fields. Why make them enter it again?
This is where the option to auto populate Tripal Contact Profile fields with those of the registration fields.
To do this, head back over to admin -> Tripal -> Tripal Contact Profile Configuration
Now go to the 'Field Options' tab and make sure to enable 'Enable automatic population of Full name, Organization and Country from user registration'
Don't forget to click save and you should be ready to go.
Now anytime there's data in the user profile fields mentioned above, that data will also be populated into the Tripal Contact Profile creation form as well!