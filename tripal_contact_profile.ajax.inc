<?php
/**
 * @file
 * Ajax integration for the Tripal Contact Profile module.
 */

/**
 * Help function to check if a user account exists by email
 *
 * @param string $email
 *   The email address or partial email address
 *
 * @return null
 *  Returns null but executes drupal_json_output
 *
 */
function tripal_contact_profile_load_useraccountexists_ajax($email) {
	$output_result['result'] = false;
	
	$email = filter_xss(check_plain($email));
	$sql = "SELECT * FROM users WHERE mail ILIKE :email LIMIT 1;";
	$results = chado_query($sql, array(':email' => $email));
	foreach($results as $result) {
		$output_result['value'] = $result->mail;
		$output_result['result'] = true;
		break;
	}
	//$output_result['sql'] = $sql;
	$output_result['row_count'] = $results->rowCount();
	//$output_result['email'] = $email;
	
	drupal_json_output($output_result);
}

/**
 * Help function to check if a TCP record exists by email (used for user registration via email field ajax)
 *
 * @param string $email
 *   The email address or partial email address
 *
 * @return null
 *  Returns null but executes drupal_json_output
 *
 */
function tripal_contact_profile_load_hastcprecord_ajax($email) {
	$output_result['result'] = false;
	
	$email = filter_xss(check_plain($email));
	$sql = "SELECT * FROM chado.contactprop WHERE value ILIKE :email LIMIT 1;";
	$results = chado_query($sql, array(':email' => $email));
	foreach($results as $result) {
		$output_result['value'] = $result->value;
		$output_result['result'] = true;
		break;
	}
	//$output_result['sql'] = $sql;
	$output_result['row_count'] = $results->rowCount();
	//$output_result['email'] = $email;
	
	drupal_json_output($output_result);
}

/**
 * Helper function to get organization listing from the database via ajax
 *
 * @param string $text
 *   Partial organization text
 *
 * @return null
 *  Returns null but executes drupal_json_output
 *
 */
function tripal_contact_profile_load_organization_autocomplete_ajax($text) {
	  $text = strtolower($text);
	  $results = array();
	  $text = check_plain(filter_xss($text));
	  $cvterm_id = tripal_contact_profile_get_cvtermid("tripal_contact","Organization");
	  //echo "DEBUG CVTERM_ID:" . $cvterm_id . "\n";
	  
	  $args = array(
		':text' => '%' . $text . '%',
		':cvterm_id' => $cvterm_id,
	  );
	  $query_results = chado_query("SELECT DISTINCT(value) FROM {contactprop} WHERE type_id=:cvterm_id AND lower(value) LIKE :text LIMIT 20;", $args);
	  
	  //print_r($query_results);
	 
	  foreach ($query_results as $row) {
		$results[$row->value] = check_plain($row->value);
	  }
	 
	  drupal_json_output($results);	
}

/**
 * Helper function to get ORCID data from ORCID website via ajax
 *
 * @param string $orcid
 *   ORCID Number to import data
 *
 * @return null
 *  Returns null but executes drupal_json_output
 *
 */
function tripal_contact_profile_load_orcid_ajax($orcid) {

	global $countries2;
	//https://pub.orcid.org/v2.1/
	//drupal_json_output($results);
	$results = array();
	$results['orcid'] = trim($orcid);
	
	
	$opts = [
		"http" => [
			"method" => "GET",
			"header" => "Accept: application/json\r\n"
		]
	];

	$context = stream_context_create($opts);
	$json_data = file_get_contents("https://pub.orcid.org/v2.1/$orcid", false, $context);
	//$json_data = file_get_contents("https://pub.orcid.org/v2.1/$orcid");
	//$json_var = json_decode($json_data);
	
	/*
	$url = "https://www.google.co.in/";   
	$ch = curl_init();    
	curl_setopt($ch,CURLOPT_URL,$url);   
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);   
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);    
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));   
	$json_data = curl_exec($ch);   
	curl_close($ch);
	*/
	
	//var_dump($json_data);
	$json_var = json_decode($json_data, true);
	//print_r($json_var['activities-summary']['employments']['employment-summary']);
	//print_r($json_var['activities-summary']['works']['group']);
	foreach($json_var['activities-summary']['works']['group'] as $doi_record) {
		//print_r($doi_record['external-ids']['external-id'][0]['external-id-value']);
		$results['edit-tpub-doi-und-0-chado-contactprop-value'] .= $doi_record['external-ids']['external-id'][0]['external-id-value'] . "\n";
	}
	$results['edit-tpub-doi-und-0-chado-contactprop-value'] = rtrim($results['edit-tpub-doi-und-0-chado-contactprop-value']);
	$results['edit-schema-name-und-0-value'] = $json_var['person']['name']['family-name']['value'] . ", " . $json_var['person']['name']['given-names']['value'];
	
	$results['edit-local-firstname-und-0-chado-contactprop-value'] = $json_var['person']['name']['given-names']['value'];
	$results['edit-local-lastname-und-0-chado-contactprop-value'] = $json_var['person']['name']['family-name']['value'];
	
	
	$results['edit-tcontact-department-und-0-chado-contactprop-value'] = $json_var['activities-summary']['employments']['employment-summary'][0]['department-name'];
	$results['edit-sep-position-und-0-chado-contactprop-value'] = $json_var['activities-summary']['employments']['employment-summary'][0]['role-title'];
	$results['edit-tcontact-organization-und-0-chado-contactprop-value'] = $json_var['activities-summary']['employments']['employment-summary'][0]['organization']['name'];
	

	//$results['edit-tcontact-country-und-0-chado-contactprop-value'] = $countries[$json_var['activities-summary']['employments']['employment-summary'][0]['organization']['address']['country']];
	$results['edit-tcontact-country-und-0-chado-contactprop-value'] = $countries2[$json_var['activities-summary']['employments']['employment-summary'][0]['organization']['address']['country']];
	
	$results['edit-tcontact-city-und-0-chado-contactprop-value'] = $json_var['activities-summary']['employments']['employment-summary'][0]['organization']['address']['city'];
	$results['edit-tcontact-state-und-0-chado-contactprop-value'] = $json_var['activities-summary']['employments']['employment-summary'][0]['organization']['address']['region'];
	drupal_json_output($results);
}

/*
Load species from database ajax
*/
/**
 * Helper function to get species of a specific genus
 *
 * @param string $genus
 *   The genus
 *
 * @return null
 *  Returns nothing but outputs data to page
 *
 */
function tripal_contact_profile_load_species_ajax($genus) {
	$genus = strtolower($genus);
	$results = array();
	$genus = check_plain(filter_xss($genus));
	$args = array(
		':genus' => '%' . $genus . '%',
	);
	if($genus != "") {
		$query_results = chado_query("SELECT species FROM {organism} WHERE lower(genus) LIKE :genus", $args);
	}
	//print_r($query_results);
	
	foreach ($query_results as $row) {
		$results[$row->species] = check_plain($row->species);
	}
	
	drupal_json_output($results);
}

/*
This function does loading of Species via AJAX among other JS
*/
/**
 * Helper function to add genus JS code to any required page by calling this function
 *
 *
 * @return null
 *  Returns nothing
 *
 */
function tripal_contact_profile_add_edit_form_js_code_function() {
	//dpm("JS CODE FUNCTION");
	//drupal_set_message(arg(0));
	//drupal_set_message(arg(1));
	//drupal_set_message(arg(2));
	//drupal_set_message(tripal_contact_profile_getMachineName_id());


	
	if(@arg(0) == 'bio_data' || (@arg(1) == 'registermulti' && @arg(2) == 'step2')) {
		
		global $us_states, $canada_provinces;
		
		$us_states_select_options = '';
		foreach ($us_states as $state) {
			$us_states_select_options .= "<option value='$state'>$state</option>";
		}
		
		$canada_provinces_select_options = '';
		foreach ($canada_provinces as $province) {
			$canada_provinces_select_options .= "<option value='$province'>$province</option>";
		}	
	
		//drupal_set_message("Correct URL detected on TCP init");
		$js = "
			function loadSpeciesAJAX(index) {
				genus = jQuery('#edit-local-genus-und-' + index + '-genus').val();
				//window.alert(genus);
				var select_container = jQuery('#edit-local-genus-und-' + index + '-species');
				select_container.html('<option>Loading... please wait</option>');				
				
				//swal('Loading...', 'Retrieving species for ' + genus + '...', 'info', {timer: 1000});
				jQuery.getJSON('../../ajax/load/organism/species/' +  genus, function( data ) {
					//window.alert(data);
					var items = [];
					items.push('- Species -');
					jQuery.each(data, function( key, val ) {
						console.log(key);  
						items.push(key);
					});
					
					
					select_container.html('');				  
					for(i=0; i < items.length; i++) {
						str = '<option value=\'' + items[i] + '\'>' + items[i] + '</option>';
						select_container.html(select_container.html() + str);
						//window.alert(select_container.html());
					}
				});
			}
			
			/* This code is used to auto populate the hidden name or title field of the form from the first name and last name fields*/
			/* It also does backward compatibility for TCP IFRAME and the REGISTRATION PAGE for EMAIL FIELD AND FULL NAME FIELD */
			jQuery(document).ready(function() {
				try{
					console.log('ready to load on type function for last name and full name!');
					var name_text = '';
					jQuery('#edit-local-firstname-und-0-chado-contactprop-value').on('input', function() {
					//jQuery(document).on('change', '#edit-local-firstname-und-0-chado-contactprop-value', function() {
						name_text = jQuery('#edit-local-lastname-und-0-chado-contactprop-value').val() + ', ' + jQuery('#edit-local-firstname-und-0-chado-contactprop-value').val();
						jQuery('#edit-schema-name-und-0-value').val(name_text);
						
						//Check if there's a parent iframe and tcp_fullname, apply firstname and lastname to fullname
						//console.log('Debug1:');
						//console.log(jQuery(parent.document).find('#edit-tcp-fullname-und-0-value'));
						try {
							jQuery(parent.document).find('#edit-tcp-fullname-und-0-value').val(name_text);
						}
						catch(err_in1) {
							console.log(err_in1);
						}
						
						//console.log('First Name Last Name trigger 1 for:' + name_text);
						//console.log('Effective Schema Name:' + jQuery('#edit-schema-name-und-0-value').val());
					});
					jQuery('#edit-local-lastname-und-0-chado-contactprop-value').on('input', function() {
					//jQuery(document).on('change', '#edit-local-lastname-und-0-chado-contactprop-value', function() {
						name_text = jQuery('#edit-local-lastname-und-0-chado-contactprop-value').val() + ', ' + jQuery('#edit-local-firstname-und-0-chado-contactprop-value').val();
						jQuery('#edit-schema-name-und-0-value').val(name_text);
						
						//Check if there's a parent iframe and tcp_fullname, apply firstname and lastname to fullname
						//console.log('Debug2:');
						//console.log(jQuery(parent.document).find('#edit-tcp-fullname-und-0-value'));						
						try {
							jQuery(parent.document).find('#edit-tcp-fullname-und-0-value').val(name_text);
						}
						catch(err_in1) {
							console.log(err_in1);
						}
						
						//console.log('First Name Last Name trigger 2 for:' + name_text);	
						//console.log('Effective Schema Name:' + jQuery('#edit-schema-name-und-0-value').val());
					});
					
					var email = '';
					jQuery('#edit-local-email-und-0-chado-contactprop-value').on('input', function() {
						email = jQuery('#edit-local-email-und-0-chado-contactprop-value').val();
						try {
							jQuery(parent.document).find('#edit-mail').val(email);
						}
						catch(err_in1) {
							console.log(err_in1);
						}						
					})
				}
				catch(err) {
					console.log(err);
				}
			});
			
			/* This code is to make sure only country USA or Canada can select a State */
			jQuery(document).ready(function() {
				try {
					if(document.getElementById('edit-tcontact-country-und-0-chado-contactprop-value').length) {
						console.log('Country on change hook activated');
						//since default is probably Afghanistan, we need to set State to Not Available and Hide it
						//jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').val('Not Available');
						jQuery('#edit-tcontact-state').hide();		
						
						jQuery('#edit-tcontact-state label').html('State / Province');
						
						if(jQuery('#edit-tcontact-country-und-0-chado-contactprop-value').val() == 'United States') {
							jQuery('#edit-tcontact-state').show();
						}
						else if(jQuery('#edit-tcontact-country-und-0-chado-contactprop-value').val() == 'Canada') {
							jQuery('#edit-tcontact-state').show();
						}
						
						jQuery('#edit-tcontact-country-und-0-chado-contactprop-value').change(
							function() {
								//console.log('TCP DEBUG:' + jQuery('#edit-tcontact-country-und-0-chado-contactprop-value').val());
								var country_selected = jQuery('#edit-tcontact-country-und-0-chado-contactprop-value').val();
								if(country_selected == 'United States') {
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').empty();
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').append(\"$us_states_select_options\");
									jQuery('#edit-tcontact-state').show();
								}
								else if (country_selected == 'Canada') {
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').empty();
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').append(\"$canada_provinces_select_options\");
									jQuery('#edit-tcontact-state').show();
								}
								else {
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').empty();
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').append(\"<option value='Not Available'>Not Available</option>\");
									jQuery('#edit-tcontact-state-und-0-chado-contactprop-value').val('Not Available');
									jQuery('#edit-tcontact-state').hide();
								}
							}
						);
					}
					

					
				}
				catch(err) {
					console.log(err);
				}
			});
		";
		
		
		
		drupal_add_js($js,'inline');
	}
}
?>